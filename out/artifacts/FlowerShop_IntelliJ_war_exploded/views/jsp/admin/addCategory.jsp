<%--
  Created by IntelliJ IDEA.
  User: Probook
  Date: 11-Dec-20
  Time: 1:27 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.bhd.util.URLConstant"%>
<html>
<head>
    <title>Thêm Danh Mục</title>
    <link rel="stylesheet" href="/views/css/admin/bootstrap.min.css">
    <link rel="stylesheet" href="/views/css/admin/admin-common.css">
    <link rel="stylesheet" href="/views/css/admin/addproduct.css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/views/assets/myfonts/myfonts.css">
</head>
<body>
<section class="my-container">
    <%@include file="include/menu.jsp"%>
    <div class="dashboard">
        <%@include file="include/header.jsp"%>
        <div class="admin-header">
            <h1>Thêm Danh Mục</h1>
        </div>

        <!-- CONTENT-CHANGE -->
        <div class="content">
            <form class="mx-5" action='<c:url value='${ URLConstant.URL_ADMIN_CATEGORY_ADD }'></c:url>' method="POST">
                <div class="form-group">
                    <label>ID Danh Mục</label>
                    <input type="text" class="form-control" name="idCategory">
                </div>
                <div class="form-group">
                    <label>Tên Danh Mục</label>
                    <input type="text" class="form-control" name="nameCategory">
                </div>
                <div class="form-group">
                    <label>Link Hình Ảnh</label>
                    <input type="text" class="form-control" name="linkCategory">
                </div>
                <a href='<c:url value='${URLConstant.URL_ADMIN_CATEGORY}'></c:url>' class="btn btn-success">Trở lại</a>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>

        <!-- END CONTENT CHANGE -->
    </div>
</section>

<!-- SCRIPT -->
<script src="/views/js/jquery-3.2.1.slim.min.js"></script>
<script src="/views/js/popper.min.js"></script>
<script src="/views/js/bootstrap.min.js"></script>


</body>
</html>
