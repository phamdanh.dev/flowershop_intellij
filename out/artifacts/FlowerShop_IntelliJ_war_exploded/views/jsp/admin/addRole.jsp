<%--
  Created by IntelliJ IDEA.
  User: Probook
  Date: 11-Dec-20
  Time: 1:14 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.bhd.util.URLConstant"%>
<html>
<head>
    <title>Thêm Tài Khoản</title>
    <link rel="stylesheet" href="/views/css/admin/bootstrap.min.css">
    <link rel="stylesheet" href="/views/css/admin/admin-common.css">
    <link rel="stylesheet" href="/views/css/admin/AddUser.css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/views/assets/myfonts/myfonts.css">
</head>
<body>
<section class="my-container">
    <%@include file="include/menu.jsp"%>
    <div class="dashboard">
        <%@include file="include/header.jsp"%>
        <div class="admin-header">
            <h1>Thêm Quyền</h1>
        </div>
        <!-- CONTENT-CHANGE -->
        <div class="content">
            <form class="mx-5" action='<c:url value='${ URLConstant.URL_ADMIN_ROLE_ADD }'></c:url>' method="POST">
                <div class="form-group">
                    <label>ID</label>
                    <input type="text" class="form-control" name="idRole">
                </div>
                <div class="form-group">
                    <label>Tên</label>
                    <input type="text" class="form-control" name="nameRole">
                </div>
                <a href='<c:url value='${URLConstant.URL_ADMIN_ROLE}'></c:url>' class="btn btn-success">Trở lại</a>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>

            <!-- END CONTENT CHANGE -->
        </div>
    </div>

</section>


<!-- SCRIPT -->
<script src="/views/js/jquery-3.2.1.slim.min.js"></script>
<script src="/views/js/popper.min.js"></script>
<script src="/views/js/bootstrap.min.js"></script>




</body>
</html>
