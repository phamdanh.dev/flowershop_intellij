<%--
  Created by IntelliJ IDEA.
  User: Probook
  Date: 11-Dec-20
  Time: 1:42 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Doanh Thu</title>
    <link rel="stylesheet" href="../../css/admin/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/admin/admin-common.css">
    <link rel="stylesheet" href="../../css/admin/doanhthu.css">
    <link rel="stylesheet" href="../../css/admin/dist/Chart.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="../../assets/myfonts/myfonts.css">
</head>
<body>
<section class="my-container">
    <div class="menu">
        <div class="menu-name">
            <h1>John Smith</h1>
            <h6>Admin</h6>
        </div>
        <div class="menu-bar">
            <a href="">Dashboard</a>
            <a href="">Đơn hàng</a>
            <a href="">Doanh Thu</a>
            <a href="">Danh Mục</a>
            <a href="">Sản Phẩm</a>
            <a href="">Sản Phẩm Giảm Giá</a>
            <a href="">Phản Hồi</a>
            <a href="">Giao Diện</a>
            <a href="">Tài Khoản</a>
        </div>
    </div>
    <div class="dashboard">
        <div class="dashboard-header">
            <div class="account">
                <a href="">Administrator</a>
                <div class="account-option">
                    <a href="">Thông Tin Tài Khoản</a>
                    <a href="">Đăng Xuất</a>
                </div>
            </div>
        </div>
        <div class="admin-header">
            <h1>Doanh Thu</h1>
        </div>
        <div class="thong-ke">
            <div class="d-container">
                <div class="thong-ke-body">
                    <div class="thong-ke-item">
                        <a href="">
                            <p class="thong-ke-num">45</p>
                            <p class="thong-ke-text">Đơn Hàng Hôm Nay</p>
                            <div class="thong-ke-a">
                                <p>Xem Chi Tiết</p>
                            </div>
                        </a>
                    </div>

                    <div class="thong-ke-item">
                        <a href="">
                            <p class="thong-ke-num">96,7tr</p>
                            <p class="thong-ke-text">Doanh Thu Hôm Nay</p>
                            <div class="thong-ke-a">
                                <p>Xem Chi Tiết</p>
                            </div>
                        </a>
                    </div>


                    <div class="thong-ke-item">
                        <a href="">
                            <p class="thong-ke-num">138</p>
                            <p class="thong-ke-text">Đơn Hàng Hôm Qua</p>
                            <div class="thong-ke-a">
                                <p>Xem Chi Tiết</p>
                            </div>
                        </a>
                    </div>

                    <div class="thong-ke-item">
                        <a href="">
                            <p class="thong-ke-num">186,9tr</p>
                            <p class="thong-ke-text">Doanh Thu Hôm Qua</p>
                            <div class="thong-ke-a">
                                <p>Xem Chi Tiết</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- CONTENT-CHANGE -->
        <div class="content">
            <div class="btn-group d-flex justify-content-center mt-5">
                <button type="button" onclick="btnChart(value)" class="btn btn-dark" value="ngay">Ngày</button>
                <button type="button" onclick="btnChart(value)" class="btn btn-dark" value="thang">Tháng</button>
                <button type="button" onclick="btnChart(value)" class="btn btn-dark" value="nam">Năm</button>
            </div>

            <div id="chartNgay" class="myChart">
                <div>
                    <h1 class="text-center mt-5">Tổng đơn hàng theo ngày</h1>
                    <div>
                        <canvas id="chartDonHangNgay"></canvas>
                    </div>
                </div>

                <div>
                    <h1 class="text-center mt-5">Doanh thu theo ngày</h1>
                    <div>
                        <canvas id="charDoanhThuNgay"></canvas>
                    </div>
                </div>
            </div>

            <div id="chartThang" class="myChart">
                <div>
                    <h1 class="text-center mt-5">Tổng đơn hàng theo tháng</h1>
                    <div>
                        <canvas id="chartDonHangThang"></canvas>
                    </div>
                </div>

                <div>
                    <h1 class="text-center mt-5">Doanh thu theo tháng</h1>
                    <div>
                        <canvas id="charDoanhThuThangx"></canvas>
                    </div>
                </div>
            </div>

            <div id="chartNam" class="myChart">
                <div>
                    <h1 class="text-center mt-5">Tổng đơn hàng theo năm</h1>
                    <div>
                        <canvas id="chartDonHangNam"></canvas>
                    </div>
                </div>

                <div>
                    <h1 class="text-center mt-5">Doanh thu theo năm</h1>
                    <div>
                        <canvas id="chartDoanhThuNam"></canvas>
                    </div>
                </div>
            </div>

        </div>

        <!-- END CONTENT CHANGE -->
    </div>

</section>


<!-- SCRIPT -->
<script src="../../js/bootstrap.min.js"></script>
<script src="../../js/jquery-3.2.1.slim.min.js"></script>
<script src="../../js/popper.min.js"></script>
<script src="../../css/css-admin/dist/Chart.min.js"></script>


<!-- NGAY -->
<script>
    var ctxdhngay = document.getElementById('chartDonHangNgay');
    var chartDonHangNgay = new Chart(ctxdhngay, {
        type: 'line',
        data: {
            labels: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30'],
            datasets: [{
                label: '# Đơn hàng ',
                data: [13, 11, 9, 15, 16, 16, 17, 22, 11, 14, 17, 16, 12, 21, 23, 21, 18, 15, 18],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
</script>

<script>
    var ctxdtngay = document.getElementById('charDoanhThuNgay');
    var charDoanhThuNgay = new Chart(ctxdtngay, {
        type: 'bar',
        data: {
            labels: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30'],
            datasets: [{
                label: 'Triệu đồng',
                data: [7, 8, 6.5, 5, 7.5, 6.5, 7.5, 8, 8, 6.5, 9.6, 5.8, 6.5, 6.7, 7.6, 7.5, 8.5, 4.7, 8.3, 5.9],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
</script>

<!-- THANG -->
<script>
    var ctxdhthang = document.getElementById('chartDonHangThang');
    var chartDonHangThang = new Chart(ctxdhthang, {
        type: 'line',
        data: {
            labels: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6', 'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'],
            datasets: [{
                label: '# Đơn hàng ',
                data: [245, 190, 180, 155, 286, 315, 308, 320, 318, 270, 135],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
</script>

<script>
    var ctxdtthang = document.getElementById('charDoanhThuThangx');
    var chartDoanhThuThang = new Chart(ctxdtthang, {
        type: 'bar',
        data: {
            labels: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6', 'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'],
            datasets: [{
                label: 'Triệu đồng',
                data: [245, 190, 180, 155, 286, 315, 308, 320, 318, 270, 135],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
</script>


<!-- NAM -->

<script>
    var ctx = document.getElementById('chartDonHangNam');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: ['2019', '2020'],
            datasets: [{
                label: '# Đơn hàng ',
                data: [2450, 2154],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
</script>

<script>
    var ctx = document.getElementById('chartDoanhThuNam');
    var myChart2 = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['2019', '2020'],
            datasets: [{
                label: 'Tỷ đồng',
                data: [1.8, 1.6],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
</script>


<script>
    function btnChart(value) {
        var myChart = document.querySelectorAll('.myChart');
        for (let i = 0; i < myChart.length; i++) {
            myChart[i].style.display = 'none';
        };
        console.log(myChart);
        if (value === 'ngay') {
            document.getElementById('chartNgay').style.display = 'block';
        };
        if (value === 'tuan') {
            document.getElementById('chartTuan').style.display = 'block';
        };
        if (value === 'thang') {
            document.getElementById('chartThang').style.display = 'block';
        };
        if (value === 'nam') {
            document.getElementById('chartNam').style.display = 'block';
        };
    };
</script>
</body>
</html>
