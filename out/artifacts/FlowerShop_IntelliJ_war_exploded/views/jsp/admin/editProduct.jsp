<%--
  Created by IntelliJ IDEA.
  User: Probook
  Date: 11-Dec-20
  Time: 1:04 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.bhd.util.URLConstant"%>
<html>
<head>
    <title>Sửa Sản Phẩm</title>
    <link rel="stylesheet" href="/views/css/admin/bootstrap.min.css">
    <link rel="stylesheet" href="/views/css/admin/admin-common.css">
    <link rel="stylesheet" href="/views/css/admin/addproduct.css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/views/assets/myfonts/myfonts.css">
</head>
<body>
<section class="my-container">
    <%@include file="include/menu.jsp"%>
    <div class="dashboard">
        <%@include file="include/header.jsp"%>
        <div class="admin-header">
            <h1>Sửa Sản Phẩm</h1>
        </div>

        <!-- CONTENT-CHANGE -->
        <div class="content">
            <form class="mx-5" action='<c:url value='${ URLConstant.URL_ADMIN_PRODUCT_EDIT }?id=${product.id}'></c:url>' method="POST">
                <div class="form-group">
                    <label>ID</label>
                    <input type="text" class="form-control" name="id" value='${product.id}' disabled>
                </div>
                <div class="form-group">
                    <label>Tên Sản Phẩm</label>
                    <input type="text" class="form-control" name="name" value='${product.name}'>
                </div>
                <div class="form-group">
                    <label>Danh Mục</label>
                    <select name="idCategory" id="idCategory">
                        <c:forEach var="item" items="${categories}">
                            <option value="${item.idCategory}">${item.nameCategory}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="form-group">
                    <label>Danh Mục Con</label>
                    <select name="idSubCategory" id="idSubCategory">
                        <option value="null">Không có mục con</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Image</label>
                    <input type="text" class="form-control" name="linkImg" value='${product.images}'>
                </div>
                <div class="form-group">
                    <label>Giá</label>
                    <input type="text" class="form-control" name="price" value='${product.price}'>
                </div>
                <div class="form-group">
                    <label>Sale</label>
                    <input type="text" class="form-control" name="sale" value='${product.sale}'>
                </div>
                <div class="form-group">
                    <label>Content</label>
                    <textarea type="text" class="form-control" name="content">${product.content}</textarea>
                </div>

                <a href='<c:url value='${URLConstant.URL_ADMIN_PRODUCT}'></c:url>' class="btn btn-success">Trở lại</a>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>

        <!-- END CONTENT CHANGE -->
    </div>
</section>

<!-- SCRIPT -->
<!-- SCRIPT -->
<script src="/views/js/jquery-3.2.1.slim.min.js"></script>
<script src="/views/js/popper.min.js"></script>
<script src="/views/js/bootstrap.min.js"></script>

<script type="text/javascript">
    $category = $('#idCategory');
    $category.change (

        function() {
            $.ajax({
                type: "GET",
                url: "/selectSubCategory",
                data: {idCategory: $category.val() },
                success: function(data){
                    $("#idSubCategory").html(data)

                }
            });
            console.log("DATA: ", $category.val());
        }

    );
</script>

</body>
</html>
