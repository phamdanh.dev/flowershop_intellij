<%--
  Created by IntelliJ IDEA.
  User: Probook
  Date: 06-Jan-21
  Time: 5:29 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.bhd.util.URLConstant"%>
<html>
<head>
    <link rel="stylesheet" href="/views/css/admin/bootstrap.min.css">
    <link rel="stylesheet" href="/views/css/admin/admin-common.css">
    <link rel="stylesheet" href="/views/css/admin/dashboard.css">
    <link rel="stylesheet" href="/views/css/admin/dist/Chart.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/views/assets/myfonts/myfonts.css">
</head>
<body>
<div class="menu">
    <div class="menu-name">
        <h1>${sessionScope.ADMIN_NAME}</h1>
        <h6>Admin</h6>
    </div>
    <div class="menu-bar">
        <a href='<c:url value='${URLConstant.URL_ADMIN_DASHBOARD}'></c:url>'>Dashboard</a>
        <a href="">Đơn hàng</a>
        <a href="">Doanh Thu</a>
        <a href='<c:url value='${URLConstant.URL_ADMIN_CATEGORY}'></c:url>'>Danh Mục</a>
        <a href='<c:url value='${URLConstant.URL_ADMIN_SUBCATEGORY}'></c:url>'>Danh Mục Con</a>
        <a href='<c:url value='${URLConstant.URL_ADMIN_PRODUCT}'></c:url>'>Sản Phẩm</a>
        <a href="">Phản Hồi</a>
        <a href='<c:url value='${URLConstant.URL_ADMIN_USER}'></c:url>'>Tài Khoản</a>
        <a href='<c:url value='${URLConstant.URL_ADMIN_ROLE}'></c:url>'>Quyền</a>
    </div>
</div>

<!-- SCRIPT -->
<script src="/views/js/jquery-3.2.1.slim.min.js"></script>
<script src="/views/js/popper.min.js"></script>
<script src="/views/js/bootstrap.min.js"></script>
</body>
</html>
