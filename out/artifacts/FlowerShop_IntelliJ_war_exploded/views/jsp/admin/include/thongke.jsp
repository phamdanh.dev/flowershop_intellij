<%--
  Created by IntelliJ IDEA.
  User: Probook
  Date: 06-Jan-21
  Time: 5:29 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.bhd.util.URLConstant"%>
<html>
<head>
    <link rel="stylesheet" href="/views/css/admin/bootstrap.min.css">
    <link rel="stylesheet" href="/views/css/admin/admin-common.css">
    <link rel="stylesheet" href="/views/css/admin/dashboard.css">
    <link rel="stylesheet" href="/views/css/admin/dist/Chart.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/views/assets/myfonts/myfonts.css">
</head>
<body>
<div class="thong-ke">
    <div class="d-container">
        <div class="thong-ke-body">
            <div class="thong-ke-item">
                <a href="">
                    <p class="thong-ke-num">15</p>
                    <p class="thong-ke-text">Đơn Hàng Cần Xử Lý</p>
                    <div class="thong-ke-a">
                        <p>Xem Chi Tiết</p>
                    </div>
                </a>
            </div>

            <div class="thong-ke-item">
                <a href="">
                    <p class="thong-ke-num">124</p>
                    <p class="thong-ke-text">Đơn Hàng Hôm Nay</p>
                    <div class="thong-ke-a">
                        <p>Xem Chi Tiết</p>
                    </div>
                </a>
            </div>


            <div class="thong-ke-item">
                <a href="">
                    <p class="thong-ke-num">23</p>
                    <p class="thong-ke-text">Sản Phẩm Giảm Giá</p>
                    <div class="thong-ke-a">
                        <p>Xem Chi Tiết</p>
                    </div>
                </a>
            </div>

            <div class="thong-ke-item">
                <a href="">
                    <p class="thong-ke-num">45</p>
                    <p class="thong-ke-text">Sản Phẩm</p>
                    <div class="thong-ke-a">
                        <p>Xem Chi Tiết</p>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>

<!-- SCRIPT -->
<script src="/views/js/jquery-3.2.1.slim.min.js"></script>
<script src="/views/js/popper.min.js"></script>
<script src="/views/js/bootstrap.min.js"></script>
</body>
</html>
