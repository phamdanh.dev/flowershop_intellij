<%--
  Created by IntelliJ IDEA.
  User: Probook
  Date: 11-Dec-20
  Time: 12:53 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Chi Tiết Tài Khoản</title>
    <link rel="stylesheet" href="../../css/admin/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/admin/admin-common.css">
    <link rel="stylesheet" href="../../css/admin/orderdetails.css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="../../assets/myfonts/myfonts.css">
</head>
<body>
<section class="my-container">
    <div class="menu">
        <div class="menu-name">
            <h1>John Smith</h1>
            <h6>Admin</h6>
        </div>
        <div class="menu-bar">
            <a href="">Dashboard</a>
            <a href="">Đơn hàng</a>
            <a href="">Doanh Thu</a>
            <a href="">Danh Mục</a>
            <a href="">Sản Phẩm</a>
            <a href="">Sản Phẩm Giảm Giá</a>
            <a href="">Phản Hồi</a>
            <a href="">Giao Diện</a>
            <a href="">Tài Khoản</a>
        </div>
    </div>
    <div class="dashboard">
        <div class="dashboard-header">
            <div class="account">
                <a href="">Administrator</a>
                <div class="account-option">
                    <a href="">Thông Tin Tài Khoản</a>
                    <a href="">Đăng Xuất</a>
                </div>
            </div>
        </div>
        <div class="admin-header">
            <h1>Chi Tiết Tài Khoản</h1>
        </div>
        <!-- CONTENT-CHANGE -->
        <div class="content">
            <div class="row bg-white" style="height: 350px; width: 100%; margin: 0;">
                <div class="col-6" style="overflow-wrap: break-word;">
                    <h5>Username: <span><a href="" style="font-size: 1.25rem;">Nguyen Hanh</a></span></h5>
                    <h5>Tên Khách Hàng: Nguyễn Thị Hạnh</h5>
                    <h5>SDT: 0908764523</h5>
                    <h5>Email:hanhnguyen@gmail.com</h5>
                    <h5>Địa chỉ: 123 Trần Hưng Đạo, P.11, Q.5, TP.HCM</h5>
                </div>
                <div class="col-6">
                    <h5>Tổng Đơn Hàng: 3 </h5>
                    <h5>Lần Mua Hàng Gần Nhất: 14/11/2020</h5>
                    <div class="row mt-5 d-flex">
                        <a href="" class="btn btn-primary mr-3 px-5">Sửa Thông Tin</a>
                        <a href="" class="btn btn-danger">Xoá Tài Khoản</a>
                    </div>
                </div>
            </div>
            <h2 class="bg-white py-4" style="text-align: center;">Lịch Sử Mua Hàng</h2>
            <table class="table table-hover">
                <thead>
                <tr class="text-center">
                    <th scope="col">STT</th>
                    <th scope="col">Mã Đơn</th>
                    <th scope="col">Ngày Tạo Đơn</th>
                    <th scope="col">Tổng Thanh Toán</th>
                    <th scope="col">Trạng Thái</th>
                    <th scope="col">#</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th scope="row">1</th>
                    <td>5-23-14112020-114511</td>
                    <td>14/11/2020</td>
                    <td>1,620,000đ</td>
                    <td>Đã Duyệt</td>
                    <td>
                        <a href="" style="color: blue;">Xem Chi Tiết</a>
                    </td>
                </tr>
                <tr>
                    <th scope="row">1</th>
                    <td>5-23-14112020-114511</td>
                    <td>14/11/2020</td>
                    <td>1,620,000đ</td>
                    <td>Đã Duyệt</td>
                    <td>
                        <a href="" style="color: blue;">Xem Chi Tiết</a>
                    </td>
                </tr>
                <tr>
                    <th scope="row">1</th>
                    <td>5-23-14112020-114511</td>
                    <td>14/11/2020</td>
                    <td>1,620,000đ</td>
                    <td>Đã Duyệt</td>
                    <td>
                        <a href="" style="color: blue;">Xem Chi Tiết</a>
                    </td>
                </tr>
                </tbody>
            </table>
            <div class="row bg-white m-0">
                <p style="padding:30px 10px 10px;">Copyright © 2020 BHD Flower. All Rights Reserved.</p>
            </div>
        </div>

        <!-- END CONTENT CHANGE -->
    </div>

</section>


<!-- SCRIPT -->
<script src="../../js/jquery-3.2.1.slim.min.js"></script>
<script src="../../js/popper.min.js"></script>
<script src="../../js/bootstrap.min.js"></script>

</body>
</html>
