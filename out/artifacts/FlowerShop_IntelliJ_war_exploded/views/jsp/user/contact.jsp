<%--
  Created by IntelliJ IDEA.
  User: Probook
  Date: 11-Dec-20
  Time: 12:13 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Liên Hệ</title>
    <link rel="stylesheet" href="../../css/user/style.css">
    <link rel="stylesheet" href="../../css/user/contact.css">
    <link rel="stylesheet" href="../../assets/external/all.css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="../../assets/myfonts/myfonts.css">
</head>
<body>
<!-- HEADER -->
<header id="header">
    <div class="container">
        <div id="header-img">
            <a href=""><img src="../../images/logo.PNG" alt="bhd-flower-logo"></a>
        </div>
        <nav>
            <ul>
                <li><a href="./home.html">Trang Chủ</a></li>
                <li id="sp-hover"><a href="./product.html">Sản Phẩm</a>

                    <div id="sp-hover-list">
                        <ul>
                            <li><a href="">HOA KHAI TRƯƠNG</a></li>
                            <li><a href="">HOA TÌNH YÊU</a></li>
                            <li><a href="">HOA CƯỚI</a></li>
                            <li><a href="">HOA LỄ</a></li>
                            <li><a href="">HOA TRANG TRÍ</a></li>
                            <li id="sp-hover-2"><a href="">HOA TƯƠI</a>
                                <div id="sp-hover-list-2">
                                    <ul>
                                        <li><a href="">HOA HỒNG</a></li>
                                        <li><a href="">HOA LY</a></li>
                                        <li><a href="">HOA SEN</a></li>
                                        <li><a href="">HOA HƯỚNG DƯƠNG</a></li>
                                        <li><a href="">HOA LAN</a></li>
                                    </ul>
                                </div>

                            </li>
                        </ul>
                    </div>
                </li>
                <li><a href="./introduction.html">Giới Thiệu</a></li>
                <li><a href="./contact.html">Liên Hệ</a></li>
            </ul>
        </nav>
        <div id="header-search">
            <form action="">
                <input type="text" name="" id="" placeholder="Bạn tìm hoa gì?">
                <button><i class="fas fa-search"></i></button>
            </form>
        </div>
        <div id="header-cart">
            <a href=""><i class="fas fa-shopping-cart"></i></a>
        </div>
        <div id="header-login">
            <i class="fas fa-lock"></i>
            <p>LOGIN</p>
            <ul>
                <li><a href="">Đăng nhập</a></li>
                <li><a href="">Đăng kí mới</a></li>
            </ul>
        </div>
    </div>
</header>
<div id="header-clear"></div>
<!-- END HEADER -->

<!-- DIRECTION -->
<section id="direction">
    <div class="container">
        <a href="">Trang Chủ</a>
        <span><i class="fas fa-caret-right"></i></span>
        <a href="">Liên Hệ</a>
    </div>
</section>

<!-- CONTENT -->
<section id="section_1">
    <div class="container">
        <div class="content">
            <div class="left">
                <h1>CÔNG TY TNHH BHD</h1>
                <p>Địa Chỉ: 231 đường số 22, khu Ao Sen , P.Bình Trị Đông B, Q.Bình Tân, TP.HCM</p>
                <p>Email: bhd98@gmail.com</p>
                <p>Hotline: 1900 0239 - 0906 449 508 - Mr BHD</p>
                <ul style="list-style: none;">
                    <li><i class="fas fa-plus-square"></i>Nhượng Quyền Cửa Hàng?</li>
                    <li><i class="fas fa-plus-square"></i>Hủy Đơn Hàng Vừa Đặt?</li>
                    <li><i class="fas fa-plus-square"></i>Thay Đổi Thời Gian Vận Chuyển?</li>
                    <li><i class="fas fa-plus-square"></i>Phương Thức Vận Chuyển?</li>
                </ul>
            </div>
            <div class="right">
                <form action="" style="list-style: none;">
                    <input type="text" placeholder="Tên của bạn"><br>
                    <input type="text" placeholder="Email" class="email">
                    <input type="text" placeholder="Phone" class="phone"><br>
                    <textarea name="" id="" cols="30" rows="10" class="textarea"
                              placeholder="Nội Dung"></textarea><br>
                    <button class="send">GỬI<i class="fas fa-long-arrow-alt-right"></i></button>
                </form>
            </div>
        </div>

    </div>

</section>
<!-- FOOTER -->
<footer>
    <div id="footer">
        <div class="container">
            <div id="footer-1">
                <div class="f-1-item">
                    <img src="../../images/logo.PNG" alt="">
                </div>
                <div class="f-1-item">
                    <h3>Hỗ Trợ</h3>
                    <ul>
                        <li><a href="">Ship Hàng Miễn Phí</a></li>
                        <li><a href="">Hướng Dẫn Mua Hàng</a></li>
                        <li><a href="">Phương Thức Thanh Toán</a></li>
                        <li><a href="">Phương Thức Giao Hàng</a></li>
                    </ul>
                </div>
                <div class="f-1-item">
                    <h3>Chăm Sóc Khách Hàng</h3>
                    <img src="../../images/logo.PNG" alt="">
                </div>
                <div class="f-1-item">
                    <h3>Liên Kết Nhanh</h3>
                    <ul>
                        <li><a href="">Trang Chủ</a></li>
                        <li><a href="">Sản Phẩm</a></li>
                        <li><a href="">Giới Thiệu</a></li>
                        <li><a href="">Liên Hệ</a></li>
                    </ul>
                </div>
            </div>
            <div id="footer-2">
                <div id="f-2-left">
                    <h3>CỬA HÀNG HOA BHD FLOWER</h3>
                    <p>Địa chỉ: 231 Đường số 32, P.Bình Trị Đông B, Q.Bình Tân, TP.HCM</p>
                    <p>Hotline: 08 9808 2836 Mr Danh</p>
                    <p>Email: bhdflower@gmail.com</p>
                </div>
                <div id="f-2-right">
                    <a href=""><i class="fab fa-instagram-square"></i></a>
                    <a href=""><i class="fab fa-facebook"></i></a>
                    <a href=""><i class="fab fa-twitter-square"></i></a>
                    <a href=""><i class="fab fa-youtube-square"></i></a>
                </div>

            </div>
            <div id="footer-3">
                <div>
                    <p>Copyright &copy 2020 BHD Flower. All Rights Reserved.</p>
                </div>
                <div>
                    <p>GitLab <i class="fab fa-git-square"></i> <i class="fab fa-gitlab"></i> : <span><a
                            href="https://tinyurl.com/y2pkz68p">https://gitlab.com/bhdflower</a></span></p>
                </div>
            </div>
        </div>
    </div>
</footer>
</body>
</html>
