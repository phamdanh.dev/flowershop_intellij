<%--
  Created by IntelliJ IDEA.
  User: Probook
  Date: 11-Dec-20
  Time: 12:22 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Chi Tiết Sản Phẩm</title>
    <link rel="stylesheet" href="../../css/user/style.css">
    <link rel="stylesheet" href="../../css/user/common.css">
    <link rel="stylesheet" href="../../css/user/productdetail.css">
    <link rel="stylesheet" href="../../assets/external/all.css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="../../assets/myfonts/myfonts.css">
</head>
<body>
<!-- HEADER -->
<header id="header">
    <div class="container">
        <div id="header-img">
            <a href=""><img src="../../images/logo.PNG" alt="bhd-flower-logo"></a>
        </div>
        <nav>
            <ul>
                <li><a href="./home.html">Trang Chủ</a></li>
                <li id="sp-hover"><a href="./product.html">Sản Phẩm</a>

                    <div id="sp-hover-list">
                        <ul>
                            <li><a href="">HOA KHAI TRƯƠNG</a></li>
                            <li><a href="">HOA TÌNH YÊU</a></li>
                            <li><a href="">HOA CƯỚI</a></li>
                            <li><a href="">HOA LỄ</a></li>
                            <li><a href="">HOA TRANG TRÍ</a></li>
                            <li id="sp-hover-2"><a href="">HOA TƯƠI</a>
                                <div id="sp-hover-list-2">
                                    <ul>
                                        <li><a href="">HOA HỒNG</a></li>
                                        <li><a href="">HOA LY</a></li>
                                        <li><a href="">HOA SEN</a></li>
                                        <li><a href="">HOA HƯỚNG DƯƠNG</a></li>
                                        <li><a href="">HOA LAN</a></li>
                                    </ul>
                                </div>

                            </li>
                        </ul>
                    </div>
                </li>
                <li><a href="./introduction.html">Giới Thiệu</a></li>
                <li><a href="./contact.html">Liên Hệ</a></li>
            </ul>
        </nav>
        <div id="header-search">
            <form action="">
                <input type="text" name="" id="" placeholder="Bạn tìm hoa gì?">
                <button><i class="fas fa-search"></i></button>
            </form>
        </div>
        <div id="header-cart">
            <a href=""><i class="fas fa-shopping-cart"></i></a>
        </div>
        <div id="header-login">
            <i class="fas fa-lock"></i>
            <p>LOGIN</p>
            <ul>
                <li><a href="">Đăng nhập</a></li>
                <li><a href="">Đăng kí mới</a></li>
            </ul>
        </div>
    </div>
</header>
<div id="header-clear"></div>
<!-- END HEADER -->

<!-- DIRECTION -->
<section id="direction">
    <div class="container">
        <a href="">Trang Chủ</a>
        <span><i class="fas fa-caret-right"></i></span>
        <a href="">Sản Phẩm</a>
        <span><i class="fas fa-caret-right"></i></span>
        <a href="">Chi Tiết Sản Phẩm</a>
    </div>
</section>

<!-- CONTENT -->
<section id="section_2">
    <div class="container">
        <h1>Chi Tiết Sản Phẩm</h1>
        <div class="display">
            <div class="left">
                <img src="../../images/product/hoa1.jpg" alt="">
            </div>
            <div class="right">
                <div class="product-info">
                    <h2>Hoa Yêu Thương 2</h2>
                    <span><i class="fas fa-tag"></i> Sale 25%</span>
                    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Iste, maxime? Dolor ipsa, neque
                        dignissimos ipsam error sed qui aperiam quia laboriosam reprehenderit, beatae veniam
                        officiis.</p>
                </div>
                <div class="product-sale">
                    <p>Giao hàng trong vòng 2h nội thành TP.HCM</p>
                    <p>Miễn phí vận chuyển tại TP.HCM</p>
                    <p>Tặng 1 phiếu giảm giá 10% cho lần mua sau</p>
                </div>
                <div class="cart-item-info-amount" style="user-select: none;">
                    <span>Số lượng: </span>
                    <i class="fas fa-minus" onclick="cartItemMinus('cart-item-amount-${1}')"></i>
                    <input id="cart-item-amount-${1}" type="number" value="1">
                    <i class="fas fa-plus" onclick="cartItemPlus('cart-item-amount-${1}')"></i>
                </div>
                <div class="btn-group">
                    <a href="" class="btn-buy btn">
                        TIẾP TỤC MUA <i class="fas fa-cart-plus"></i>
                    </a>
                    <a href="" class="btn-pay btn">
                        THANH TOÁN NGAY <i class="far fa-credit-card"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- DANH SACH SAN PHAM -->
<section id="sp">
    <div class="container">
        <h1 class="product-list-h1">Sản Phẩm Liên Quan</h1>
        <div class="product-list">
            <div class="product-item">
                <a href=""><img src="../../images/logo.PNG" alt=""></a>
                <br>
                <a href="">Yêu thương 1</a>
                <div class="product-item-price">
                    <span>200,000 VND</span>
                    <span><i class="fas fa-tag"></i> Sale 25%</i></span>
                </div>
                <div class="product-item-btn">
                    <a href="">Thêm vào giỏ</a>
                    <a href="">Mua ngay</a>
                </div>
            </div>
            <div class="product-item">
                <a href=""><img src="../../images/logo.PNG" alt=""></a>
                <br>
                <a href="">Yêu thương 1</a>
                <div class="product-item-price">
                    <span>200,000 VND</span>
                    <span><i class="fas fa-tag"></i> Sale 25%</i></span>
                </div>
                <div class="product-item-btn">
                    <a href="">Thêm vào giỏ</a>
                    <a href="">Mua ngay</a>
                </div>
            </div>
            <div class="product-item">
                <a href=""><img src="../../images/logo.PNG" alt=""></a>
                <br>
                <a href="">Yêu thương 1</a>
                <div class="product-item-price">
                    <span>200,000 VND</span>
                    <span><i class="fas fa-tag"></i> Sale 25%</i></span>
                </div>
                <div class="product-item-btn">
                    <a href="">Thêm vào giỏ</a>
                    <a href="">Mua ngay</a>
                </div>
            </div>
            <div class="product-item">
                <a href=""><img src="../../images/logo.PNG" alt=""></a>
                <br>
                <a href="">Yêu thương 1</a>
                <div class="product-item-price">
                    <span>200,000 VND</span>
                    <span><i class="fas fa-tag"></i> Sale 25%</i></span>
                </div>
                <div class="product-item-btn">
                    <a href="">Thêm vào giỏ</a>
                    <a href="">Mua ngay</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- CONTACT -->
<div id="footer-contact">
    <div class="container">
        <h1>Liên Hệ</h1>
        <div id="f-contact-list">
            <div class="f-contact-item">
                <h3>Địa Chỉ</h3>
                <p>231 Đường số 32, P.Bình Trị Đông B, Q.Bình Tân, TP.HCM</p>
            </div>
            <div class="f-contact-item">
                <h3>Số Điện Thoại</h3>
                <p>08 9808 2836 - Mr.Danh</p>
            </div>
            <div class="f-contact-item">
                <h3>Email</h3>
                <p>contact.bhdflower@gmail.com</p>
            </div>
            <div class="f-contact-item">
                <h3>Giờ Làm Việc</h3>
                <p>5h30 - 22h30</p>
                <p>Tất cả các ngày trong tuần</p>
            </div>
        </div>
    </div>
</div>

<!-- MAP -->
<iframe
        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.7843494480253!2d106.6064945142869!3d10.751096762601838!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752c34d6561c0f%3A0xc0abbaf11df72ff6!2zMjMxIMSQxrDhu51uZyBz4buRIDMyIFRp4buDdSBLaHUgMSwgQsOsbmggVHLhu4sgxJDDtG5nIEIsIELDrG5oIFTDom4sIFRow6BuaCBwaOG7kSBI4buTIENow60gTWluaCwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1603097034851!5m2!1svi!2s"
        width="100%" height="320" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false"
        tabindex="0"></iframe>


<!-- FOOTER -->
<footer>
    <div id="footer">
        <div class="container">
            <div id="footer-1">
                <div class="f-1-item">
                    <img src="../../images/logo.PNG" alt="">
                </div>
                <div class="f-1-item">
                    <h3>Hỗ Trợ</h3>
                    <ul>
                        <li><a href="">Ship Hàng Miễn Phí</a></li>
                        <li><a href="">Hướng Dẫn Mua Hàng</a></li>
                        <li><a href="">Phương Thức Thanh Toán</a></li>
                        <li><a href="">Phương Thức Giao Hàng</a></li>
                    </ul>
                </div>
                <div class="f-1-item">
                    <h3>Chăm Sóc Khách Hàng</h3>
                    <img src="../../images/logo.PNG" alt="">
                </div>
                <div class="f-1-item">
                    <h3>Liên Kết Nhanh</h3>
                    <ul>
                        <li><a href="">Trang Chủ</a></li>
                        <li><a href="">Sản Phẩm</a></li>
                        <li><a href="">Giới Thiệu</a></li>
                        <li><a href="">Liên Hệ</a></li>
                    </ul>
                </div>
            </div>
            <div id="footer-2">
                <div id="f-2-left">
                    <h3>CỬA HÀNG HOA BHD FLOWER</h3>
                    <p>Địa chỉ: 231 Đường số 32, P.Bình Trị Đông B, Q.Bình Tân, TP.HCM</p>
                    <p>Hotline: 08 9808 2836 Mr Danh</p>
                    <p>Email: bhdflower@gmail.com</p>
                </div>
                <div id="f-2-right">
                    <a href=""><i class="fab fa-instagram-square"></i></a>
                    <a href=""><i class="fab fa-facebook"></i></a>
                    <a href=""><i class="fab fa-twitter-square"></i></a>
                    <a href=""><i class="fab fa-youtube-square"></i></a>
                </div>

            </div>
            <div id="footer-3">
                <div>
                    <p>Copyright &copy 2020 BHD Flower. All Rights Reserved.</p>
                </div>
                <div>
                    <p>GitLab <i class="fab fa-git-square"></i> <i class="fab fa-gitlab"></i> : <span><a
                            href="https://tinyurl.com/y2pkz68p">https://gitlab.com/bhdflower</a></span></p>
                </div>
            </div>
        </div>
    </div>



</footer>
<script src="../../js/jquery-3.5.1.min.js"></script>
<script>
    const cartItemMinus = (id) => {
        let num = parseInt(document.getElementById(id).value)
        if (num > 1) {
            document.getElementById(id).value = num - 1
        }
    }

    const cartItemPlus = (id) => {
        let num = parseInt(document.getElementById(id).value)
        document.getElementById(id).value = num + 1

    }

    $('.cart-item-del').click(function (event) {
        event.preventDefault();
        var r = confirm("Bạn có chắc muốn xoá sản phẩm này?");
        if (r == true) {
            window.location = $(this).attr('href');
        }

    });


</script>
</body>
</html>
