<%--
  Created by IntelliJ IDEA.
  User: Probook
  Date: 11-Dec-20
  Time: 11:47 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.bhd.util.URLConstant"%>
<html>
<head>
    <title>Tài Khoản</title>
    <link rel="stylesheet" href="/views/css/user/style.css">
    <link rel="stylesheet" href="/views/css/user/common.css">
    <link rel="stylesheet" href="/views/css/user/user.css">
    <link rel="stylesheet" href="/views/assets/external/all.css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/views/assets/myfonts/myfonts.css">
</head>
<body>
<!-- HEADER -->
<%@include file="./include/header.jsp"%>
<!-- END HEADER -->

<!-- DIRECTION -->
<section id="direction">
    <div class="container">
        <a href="">Trang Chủ</a>
        <span><i class="fas fa-caret-right"></i></span>
        <a href="">Tài Khoản</a>
    </div>
</section>

<!-- CONTENT -->
<section id="user">
    <div class="container">
        <h1>Thông Tin Khách Hàng</h1>
        <div class="content">
            <div class="u-menu">
                <div class="u-name">
                    <span>John Smith</span>
                </div>
                <a href='<c:url value='${URLConstant.URL_USER_ACCOUNT}'></c:url>'>Tài Khoản</a>
                <a href='<c:url value='${URLConstant.URL_USER_HISTORY}'></c:url>'>Lịch Sử Mua Hàng</a>
            </div>
            <div class="u-form">
                <form action='<c:url value='${ URLConstant.URL_USER_ACCOUNT_EDIT }'></c:url>' method="POST">
                    <div class="u-form-input">
                        <span>Tên Khách Hàng: </span><input type="text" disabled value='${user.name}' name="name">
                    </div>
                    <div class="u-form-input">
                        <span>Số Điện Thoại: </span><input type="text" disabled value='${user.phone}' name="phone">
                    </div>
                    <div class="u-form-input">
                        <span>Mật Khẩu: </span><input type="text" disabled value='${user.password}' name="password">
                    </div>
                    <div class="u-form-input">
                        <span>Email: </span><input type="email" disabled value='${user.email}' name="email">
                    </div>
                    <div class="u-form-input">
                        <span>Địa Chỉ: </span><input type="text" disabled
                                                     value='${user.address}' name="address">
                    </div>
                    <span id="edit-info" class="u-form-btn">Sửa Thông Tin</span>
                    <a href="" id="reset-info" class="u-form-btn" style="display: none;">Huỷ</a>
                    <button id="save-info" class="u-form-btn" type="submit" style="display: none;">Lưu Thông Tin</button>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- CONTACT -->
<div id="footer-contact">
    <div class="container">
        <h1>Liên Hệ</h1>
        <div id="f-contact-list">
            <div class="f-contact-item">
                <h3>Địa Chỉ</h3>
                <p>231 Đường số 32, P.Bình Trị Đông B, Q.Bình Tân, TP.HCM</p>
            </div>
            <div class="f-contact-item">
                <h3>Số Điện Thoại</h3>
                <p>08 9808 2836 - Mr.Danh</p>
            </div>
            <div class="f-contact-item">
                <h3>Email</h3>
                <p>contact.bhdflower@gmail.com</p>
            </div>
            <div class="f-contact-item">
                <h3>Giờ Làm Việc</h3>
                <p>5h30 - 22h30</p>
                <p>Tất cả các ngày trong tuần</p>
            </div>
        </div>
    </div>
</div>

<!-- FOOTER -->
<%@include file="./include/footer.jsp"%>

<script>
    document.getElementById('edit-info').onclick = () => {
        document.getElementById('edit-info').style.display = 'none';
        document.getElementById('reset-info').style.display = 'inline-block';
        document.getElementById('save-info').style.display = 'inline-block';

        let inputArr = document.querySelectorAll('.u-form input');
        for (let i = 0; i < inputArr.length; i++) {
            inputArr[i].style.backgroundColor = '#cccccc1c';
            inputArr[i].disabled = false;
        }
    }
</script>
</body>
</html>
