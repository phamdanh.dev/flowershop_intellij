<%--
  Created by IntelliJ IDEA.
  User: Probook
  Date: 11-Dec-20
  Time: 11:42 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.bhd.util.URLConstant"%>
<html>
<head>
    <title>Lịch Sử Mua Hàng</title>
    <link rel="stylesheet" href="/views/css/user/style.css">
    <link rel="stylesheet" href="/views/css/user/common.css">
    <link rel="stylesheet" href="/views/css/user/user.css">
    <link rel="stylesheet" href="/views/assets/external/all.css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/views/assets/myfonts/myfonts.css">
</head>
<body>
<!-- HEADER -->
<%@include file="./include/header.jsp"%>
<!-- END HEADER -->

<!-- DIRECTION -->
<section id="direction">
    <div class="container">
        <a href="">Trang Chủ</a>
        <span><i class="fas fa-caret-right"></i></span>
        <a href="">Tài Khoản</a>
    </div>
</section>

<!-- CONTENT -->
<section id="user">
    <div class="container">
        <h1>Lịch Sử Mua Hàng</h1>
        <div class="content">
            <div class="u-menu">
                <div class="u-name">
                    <span>John Smith</span>
                </div>
                <a href='<c:url value='${URLConstant.URL_USER_ACCOUNT}'></c:url>'>Tài Khoản</a>
                <a href='<c:url value='${URLConstant.URL_USER_HISTORY}'></c:url>'>Lịch Sử Mua Hàng</a>
            </div>
            <div class="u-form">
                <div id="order-list">
                    <div class="order-item">
                        <div class="order-top">
                            <p>Ngày Đặt Hàng: <span>25 - 10 - 2020</span></p>
                            <p>ID Đơn Hàng: <span>10293</span></p>
                        </div>
                        <div class="order-mid">
                            <div class="order-product">
                                <div class="order-product-img">
                                    <img src="/views/images/product/hoa1.jpg" alt="">
                                </div>
                                <div class="order-product-info">
                                    <a href="">Hoa Yêu Thương 1</a>
                                    <p>x 1</p>
                                    <p>Đơn giá: <span>200.000đ</span></p>
                                </div>
                                <div class="order-product-price">
                                    <span>200.000đ</span>
                                </div>
                            </div>
                            <div class="order-product">
                                <div class="order-product-img">
                                    <img src="/views/images/product/hoa1.jpg" alt="">
                                </div>
                                <div class="order-product-info">
                                    <a href="">Hoa Yêu Thương 1</a>
                                    <p>x 1</p>
                                    <p>Đơn giá: <span>200.000đ</span></p>
                                </div>
                                <div class="order-product-price">
                                    <span>200.000đ</span>
                                </div>
                            </div>
                        </div>
                        <div class="order-bottom">
                            <div class="order-address">
                                <h6>Địa Chỉ Người Nhận</h6>
                                <p>John Smith</p>
                                <p>Số Điện Thoại: 0975634574</p>
                                <p>123 Nguyen Hue, P.Ben Nghe, Q.1, TP.HCM</p>
                            </div>
                            <div class="order-price">
                                <p>Tổng số tiền: <span>200.000đ</span></p>
                            </div>
                        </div>
                    </div>
                    <div class="order-item">
                        <div class="order-top">
                            <p>Ngày Đặt Hàng: <span>25 - 10 - 2020</span></p>
                            <p>ID Đơn Hàng: <span>10293</span></p>
                        </div>
                        <div class="order-mid">
                            <div class="order-product">
                                <div class="order-product-img">
                                    <img src="/views/images/product/hoa1.jpg" alt="">
                                </div>
                                <div class="order-product-info">
                                    <a href="">Hoa Yêu Thương 1</a>
                                    <p>x 1</p>
                                    <p>Đơn giá: <span>200.000đ</span></p>
                                </div>
                                <div class="order-product-price">
                                    <span>200.000đ</span>
                                </div>
                            </div>
                            <div class="order-product">
                                <div class="order-product-img">
                                    <img src="/views/images/product/hoa1.jpg" alt="">
                                </div>
                                <div class="order-product-info">
                                    <a href="">Hoa Yêu Thương 1</a>
                                    <p>x 1</p>
                                    <p>Đơn giá: <span>200.000đ</span></p>
                                </div>
                                <div class="order-product-price">
                                    <span>200.000đ</span>
                                </div>
                            </div>
                        </div>
                        <div class="order-bottom">
                            <div class="order-address">
                                <h6>Địa Chỉ Người Nhận</h6>
                                <p>John Smith</p>
                                <p>Số Điện Thoại: 0975634574</p>
                                <p>123 Nguyen Hue, P.Ben Nghe, Q.1, TP.HCM</p>
                            </div>
                            <div class="order-price">
                                <p>Tổng số tiền: <span>200.000đ</span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- CONTACT -->
<div id="footer-contact">
    <div class="container">
        <h1>Liên Hệ</h1>
        <div id="f-contact-list">
            <div class="f-contact-item">
                <h3>Địa Chỉ</h3>
                <p>231 Đường số 32, P.Bình Trị Đông B, Q.Bình Tân, TP.HCM</p>
            </div>
            <div class="f-contact-item">
                <h3>Số Điện Thoại</h3>
                <p>08 9808 2836 - Mr.Danh</p>
            </div>
            <div class="f-contact-item">
                <h3>Email</h3>
                <p>contact.bhdflower@gmail.com</p>
            </div>
            <div class="f-contact-item">
                <h3>Giờ Làm Việc</h3>
                <p>5h30 - 22h30</p>
                <p>Tất cả các ngày trong tuần</p>
            </div>
        </div>
    </div>
</div>

<!-- FOOTER -->
<%@include file="./include/footer.jsp"%>
</body>
</html>
