package com.bhd.controller;

import com.bhd.util.PathConstant;
import com.bhd.util.URLConstant;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = {URLConstant.URL_ADMIN_DASHBOARD})
public class AdminController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getServletPath();
        switch (action) {
            case URLConstant.URL_ADMIN_DASHBOARD: {
                req.getRequestDispatcher(PathConstant.ADMIN_DASHBOARD).forward(req, resp);
                break;
            }
        }
    }
}
