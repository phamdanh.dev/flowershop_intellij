package com.bhd.controller;

import com.bhd.dao.UserDAO;
import com.bhd.entity.User;
import com.bhd.util.PathConstant;
import com.bhd.util.URLConstant;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(urlPatterns = {URLConstant.URL_LOGIN, URLConstant.URL_ADMIN_LOGIN, URLConstant.URL_LOGOUT, URLConstant.URL_ADMIN_LOGOUT})
public class AuthController extends HttpServlet {

    private UserDAO userDAO = new UserDAO();
    private User user = new User();


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getServletPath();
        HttpSession session;
        switch (action) {
            case URLConstant.URL_LOGIN:
                req.getRequestDispatcher(PathConstant.LOGIN).forward(req, resp);
                break;

            case URLConstant.URL_ADMIN_LOGIN:
                req.getRequestDispatcher(PathConstant.ADMIN_LOGIN).forward(req, resp);
                break;

            case URLConstant.URL_LOGOUT:
                session = req.getSession();
                session.removeAttribute("USER_LOGIN");
                session.removeAttribute("USER_NAME");
                resp.sendRedirect(req.getContextPath() + URLConstant.URL_LOGIN);
                break;

            case URLConstant.URL_ADMIN_LOGOUT:
                session = req.getSession();
                session.removeAttribute("ADMIN_LOGIN");
                session.removeAttribute("ADMIN_NAME");
                resp.sendRedirect(req.getContextPath() + URLConstant.URL_ADMIN_LOGIN);
                break;
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getServletPath();

        switch (action) {
            case URLConstant.URL_LOGIN: {
                String username = req.getParameter("username");
                String password = req.getParameter("password");
                User user;

                if ((user = userDAO.findUserByPhone(username)) != null) {
                    System.out.println(user.getPassword());
                    if (user.getPassword().equals(password)) {
                        HttpSession session = req.getSession();
                        session.setAttribute("USER_LOGIN", user);
                        session.setAttribute("USER_NAME", user.getName());
                        resp.sendRedirect(req.getContextPath() + URLConstant.URL_HOME);
                    }
                } else {
                    resp.sendRedirect(req.getContextPath() + URLConstant.URL_LOGIN);

                }
                break;
            }

            case URLConstant.URL_ADMIN_LOGIN: {
                String username = req.getParameter("username");
                String password = req.getParameter("password");
                User user;

                if ((user = userDAO.findAdminByPhone(username)) != null) {
                    System.out.println(user.getPassword());
                    if (user.getPassword().equals(password)) {
                        HttpSession session = req.getSession();
                        session.setAttribute("ADMIN_LOGIN", user);
                        session.setAttribute("ADMIN_NAME", user.getName());
                        resp.sendRedirect(req.getContextPath() + URLConstant.URL_ADMIN_DASHBOARD);
                    }
                } else {
                    resp.sendRedirect(req.getContextPath() + URLConstant.URL_ADMIN_LOGIN);
                }
                break;
            }
        }
    }
}
