package com.bhd.controller;

import com.bhd.dao.CategoryDAO;
import com.bhd.entity.Category;
import com.bhd.entity.Role;
import com.bhd.entity.SubCategory;
import com.bhd.util.PathConstant;
import com.bhd.util.URLConstant;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(urlPatterns = {URLConstant.URL_ADMIN_CATEGORY, URLConstant.URL_ADMIN_CATEGORY_ADD,
        URLConstant.URL_ADMIN_CATEGORY_EDIT, URLConstant.URL_ADMIN_CATEGORY_DELETE, URLConstant.URL_ADMIN_SUBCATEGORY, URLConstant.URL_ADMIN_SUBCATEGORY_ADD,
        URLConstant.URL_ADMIN_SUBCATEGORY_EDIT, URLConstant.URL_ADMIN_SUBCATEGORY_DELETE})
public class CategoryController extends HttpServlet {

    CategoryDAO categoryDAO;

    @Override
    public void init() throws ServletException {
        categoryDAO = new CategoryDAO();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getServletPath();
        switch (action) {
            case URLConstant.URL_ADMIN_CATEGORY: {
                ArrayList<Category> categories = categoryDAO.getAllCategory();
                req.setAttribute("categories", categories);
                req.getRequestDispatcher(PathConstant.ADMIN_CATEGORY).forward(req, resp);
                break;
            }

            case URLConstant.URL_ADMIN_CATEGORY_ADD: {
                req.getRequestDispatcher(PathConstant.ADMIN_CATEGORY_ADD).forward(req, resp);
                break;
            }
            case URLConstant.URL_ADMIN_CATEGORY_EDIT: {
                String idCategory = req.getParameter("idCategory");
                Category category = categoryDAO.findCategoryById(idCategory);
                req.setAttribute("category", category);
                req.getRequestDispatcher(PathConstant.ADMIN_CATEGORY_EDIT).forward(req, resp);
                break;
            }
            case URLConstant.URL_ADMIN_CATEGORY_DELETE: {
                String idCategory = req.getParameter("idCategory");
                categoryDAO.deleteCategoryById(idCategory);
                resp.sendRedirect(req.getContextPath() + URLConstant.URL_ADMIN_CATEGORY);
                break;
            }

            case URLConstant.URL_ADMIN_SUBCATEGORY: {
                ArrayList<SubCategory> subCategories = categoryDAO.getAllSubCategory();
                req.setAttribute("subCategories", subCategories);
                req.getRequestDispatcher(PathConstant.ADMIN_SUBCATEGORY).forward(req, resp);
                break;
            }

            case URLConstant.URL_ADMIN_SUBCATEGORY_ADD: {
                ArrayList<Category> categories = categoryDAO.getAllCategory();
                req.setAttribute("categories", categories);
                req.getRequestDispatcher(PathConstant.ADMIN_SUBCATEGORY_ADD).forward(req, resp);
                break;
            }
            case URLConstant.URL_ADMIN_SUBCATEGORY_EDIT: {

                String idSubCategory = req.getParameter("idSubCategory");
                SubCategory subCategory = categoryDAO.findSubCategoryById(idSubCategory);
                req.setAttribute("subCategory", subCategory);
                ArrayList<Category> categories = categoryDAO.getAllCategory();
                req.setAttribute("categories", categories);
                req.getRequestDispatcher(PathConstant.ADMIN_SUBCATEGORY_EDIT).forward(req, resp);
                break;
            }
            case URLConstant.URL_ADMIN_SUBCATEGORY_DELETE: {
                String idSubCategory = req.getParameter("idSubCategory");
                categoryDAO.deleteSubCategoryById(idSubCategory);
                resp.sendRedirect(req.getContextPath() + URLConstant.URL_ADMIN_SUBCATEGORY);
                break;
            }

        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getServletPath();
        switch (action) {
            case URLConstant.URL_ADMIN_CATEGORY_ADD: {
                String idCategory = req.getParameter("idCategory");
                String nameCategory = req.getParameter("nameCategory");
                String linkCategory = req.getParameter("linkCategory");

                Category category = new Category();
                category.setIdCategory(idCategory);
                category.setNameCategory(nameCategory);
                category.setLinkCategory(linkCategory);

                boolean result = categoryDAO.addCategory(category);

                if (!result) {
                    System.out.println("ADD CATEGORY ERROR");
                }

                resp.sendRedirect(req.getContextPath() + URLConstant.URL_ADMIN_CATEGORY);
                break;
            }

            case URLConstant.URL_ADMIN_SUBCATEGORY_ADD: {
                String idSubCategory = req.getParameter("idSubCategory");
                String idCategory = req.getParameter("idCategory");
                String nameSubCategory = req.getParameter("nameSubCategory");

                SubCategory subCategory = new SubCategory();
                subCategory.setIdSubCategory(idSubCategory);
                subCategory.setIdCategory(idCategory);
                subCategory.setNameSubCategory(nameSubCategory);

                boolean result = categoryDAO.addSubCategory(subCategory);

                if (!result) {
                    System.out.println("ADD SUBCATEGORY ERROR");
                }

                resp.sendRedirect(req.getContextPath() + URLConstant.URL_ADMIN_SUBCATEGORY);
                break;
            }

            case URLConstant.URL_ADMIN_CATEGORY_EDIT: {
                String idCategory = req.getParameter("idCategory");
                String nameCategory = req.getParameter("nameCategory");
                String linkCategory = req.getParameter("linkCategory");

                Category category = new Category();
                category.setIdCategory(idCategory);
                category.setNameCategory(nameCategory);
                category.setLinkCategory(linkCategory);

                boolean result = categoryDAO.editCategory(category);

                if (!result) {
                    System.out.println("EDIT CATEGORY ERROR");
                }

                resp.sendRedirect(req.getContextPath() + URLConstant.URL_ADMIN_CATEGORY);
                break;
            }

            case URLConstant.URL_ADMIN_SUBCATEGORY_EDIT: {
                String idSubCategory = req.getParameter("idSubCategory");
                String idCategory = req.getParameter("idCategory");
                String nameSubCategory = req.getParameter("nameSubCategory");

                SubCategory subCategory = new SubCategory();
                subCategory.setIdSubCategory(idSubCategory);
                subCategory.setIdCategory(idCategory);
                subCategory.setNameSubCategory(nameSubCategory);

                boolean result = categoryDAO.editSubCategory(subCategory);

                if (!result) {
                    System.out.println("ADD SUBCATEGORY ERROR");
                }

                resp.sendRedirect(req.getContextPath() + URLConstant.URL_ADMIN_SUBCATEGORY);
                break;
            }


        }
    }
}
