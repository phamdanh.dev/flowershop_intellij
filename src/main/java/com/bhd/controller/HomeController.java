package com.bhd.controller;

import com.bhd.dao.CategoryDAO;
import com.bhd.dao.HomeDAO;
import com.bhd.dao.ProductDAO;
import com.bhd.dto.ProductDTO;
import com.bhd.entity.Category;
import com.bhd.entity.Product;
import com.bhd.util.PathConstant;
import com.bhd.util.URLConstant;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

@WebServlet(urlPatterns = {URLConstant.URL_HOME, URLConstant.URL_PRODUCT_CATEGORY})
public class HomeController extends HttpServlet {

    HomeDAO homeDAO;
    CategoryDAO categoryDAO;
    ProductDAO productDAO;

    @Override
    public void init() throws ServletException {
        homeDAO = new HomeDAO();
        categoryDAO = new CategoryDAO();
        productDAO = new ProductDAO();
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getServletPath();
        HttpSession session = req.getSession();

        ArrayList<Category> categories = categoryDAO.getAllCategory();
        req.setAttribute("categories", categories);

        switch (action){
            case URLConstant.URL_HOME: {
//                ArrayList<Category> listChuDe = homeDAO.getChuDe();
//                req.setAttribute("listChuDe", listChuDe);
//
//                ArrayList<Product> listNewProduct = homeDAO.getNewProducts();
//                req.setAttribute("listNewProduct", listNewProduct);
//
//                ArrayList<Product> listBestSellingProducts = homeDAO.getBestSellingProducts();
//                req.setAttribute("listBestSellingProducts", listBestSellingProducts);
//
//                ArrayList<Product> listHoaKhaiTruong = homeDAO.getHoaKhaiTruong();
//                req.setAttribute("listHoaKhaiTruong", listHoaKhaiTruong);
//
//                ArrayList<Product> listHoaTinhYeu = homeDAO.getHoaTinhYeu();
//                req.setAttribute("listHoaTinhYeu", listHoaTinhYeu);
//
//                ArrayList<Product> listHoaCuoi = homeDAO.getHoaCuoi();
//                req.setAttribute("listHoaCuoi", listHoaCuoi);
//
//                ArrayList<Product> listHoaLe = homeDAO.getHoaLe();
//                req.setAttribute("listHoaLe", listHoaLe);
//
//                ArrayList<Product> listHoaTrangTri = homeDAO.getHoaTrangTri();
//                req.setAttribute("listHoaTrangTri", listHoaTrangTri);
//
//                ArrayList<Product> listHoaTuoi = homeDAO.getHoaTuoi();
//                req.setAttribute("listHoaTuoi", listHoaTuoi);



                //get product with category
                ArrayList<ProductDTO> listHoaKhaiTruong = productDAO.get4ProductDTOsByNameCategory("Hoa Khai Trương");
                ArrayList<ProductDTO> listHoaTinhYeu = productDAO.get4ProductDTOsByNameCategory("Hoa Tình Yêu");
                ArrayList<ProductDTO> listHoaCuoi = productDAO.get4ProductDTOsByNameCategory("Hoa Cưới");
                ArrayList<ProductDTO> listHoaLe = productDAO.get4ProductDTOsByNameCategory("Hoa Lễ");
                ArrayList<ProductDTO> listHoaTrangTri = productDAO.get4ProductDTOsByNameCategory("Hoa Trang Trí");
                ArrayList<ProductDTO> listHoaTuoi = productDAO.get4ProductDTOsByNameCategory("Hoa Tươi");

                req.setAttribute("listHoaKhaiTruong", listHoaKhaiTruong);
                req.setAttribute("listHoaTinhYeu", listHoaTinhYeu);
                req.setAttribute("listHoaCuoi", listHoaCuoi);
                req.setAttribute("listHoaLe", listHoaLe);
                req.setAttribute("listHoaTrangTri", listHoaTrangTri);
                req.setAttribute("listHoaTuoi", listHoaTuoi);

                req.getRequestDispatcher(PathConstant.HOME).forward(req, resp);
                break;

            }

            case URLConstant.URL_PRODUCT_CATEGORY: {
                String idCategory = req.getParameter("idCategory");
                ArrayList<ProductDTO> products = productDAO.getProductDTOsByIdCategory(idCategory);
                req.setAttribute("products", products);
                req.getRequestDispatcher(PathConstant.PRODUCT).forward(req, resp);
                break;
            }


        }
    }

}
