package com.bhd.controller;

import com.bhd.dao.CategoryDAO;
import com.bhd.dao.ProductAdminDAO;
import com.bhd.dto.ProductDTO;
import com.bhd.entity.Category;
import com.bhd.entity.Product;
import com.bhd.entity.SubCategory;
import com.bhd.util.PathConstant;
import com.bhd.util.URLConstant;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

@WebServlet(urlPatterns = {URLConstant.URL_ADMIN_PRODUCT, URLConstant.URL_ADMIN_PRODUCT_ADD, URLConstant.URL_ADMIN_PRODUCT_EDIT,
        URLConstant.URL_ADMIN_PRODUCT_DELETE, URLConstant.URL_ADMIN_PRODUCT_CHANGESTATE, URLConstant.AJAX_SUBCATEGORY})
public class ProductAdminController extends HttpServlet {

    ProductAdminDAO productAdminDAO;
    CategoryDAO categoryDAO;
    SimpleDateFormat simpleDateFormat;

    @Override
    public void init() throws ServletException {
        productAdminDAO = new ProductAdminDAO();
        categoryDAO = new CategoryDAO();
        simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getServletPath();
        switch (action) {
            case URLConstant.URL_ADMIN_PRODUCT: {
                ArrayList<ProductDTO> product = productAdminDAO.getAllProductDto();
                req.setAttribute("product", product);
                req.getRequestDispatcher(PathConstant.ADMIN_PRODUCT).forward(req, resp);
                break;
            }
            case URLConstant.URL_ADMIN_PRODUCT_ADD: {
                ArrayList<Category> categories = CategoryDAO.getAllCategory();
                req.setAttribute("categories", categories);
                req.getRequestDispatcher(PathConstant.ADMIN_PRODUCT_ADD).forward(req, resp);
                break;
            }

            case URLConstant.AJAX_SUBCATEGORY: {

                resp.setContentType("text/html;charset=UTF-8");
                PrintWriter out = resp.getWriter();
                try {
                    String idCategory = req.getParameter("idCategory");
                    ArrayList<SubCategory> subCategories = categoryDAO.getSubCategoryByIdCategory(idCategory);
                    if (subCategories.size() > 0) {
                        String result = "";
                        for (SubCategory item : subCategories) {
                            result += "<option value=" + item.getIdSubCategory() + ">" + item.getNameSubCategory() + "</option>";
                        }
                        out.print(result);
                    } else {
                        out.print("<option value=null>Không có mục con</option>");
                    }
                }  catch (Exception ex) {
                    out.print("Error getting SubCategory name..." + ex.toString());
                }
                finally {
                    out.close();
                }
            }

            case URLConstant.URL_ADMIN_PRODUCT_EDIT: {
                ArrayList<Category> categories = CategoryDAO.getAllCategory();
                req.setAttribute("categories", categories);
                ProductDTO product = productAdminDAO.getProductDTOById(req.getParameter("idProduct"));
                req.setAttribute("product", product);
                req.getRequestDispatcher(PathConstant.ADMIN_PRODUCT_EDIT).forward(req, resp);
                break;
            }

            case URLConstant.URL_ADMIN_PRODUCT_DELETE: {
                productAdminDAO.deleteImageByIdProduct(req.getParameter("idProduct"));
                productAdminDAO.deleteProductById(req.getParameter("idProduct"));
                resp.sendRedirect(req.getContextPath() + URLConstant.URL_ADMIN_PRODUCT);
                break;
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getServletPath();
        switch (action) {
            case URLConstant.URL_ADMIN_PRODUCT_ADD: {
                String id = req.getParameter("id");
                String name = req.getParameter("name");
                String idCategory = req.getParameter("idCategory");
                String idSubCategory = req.getParameter("idSubCategory");
                double price = Double.parseDouble(req.getParameter("price"));
                double sale = Double.parseDouble(req.getParameter("sale"));
                String addDate = simpleDateFormat.format(new Date());
                String content = req.getParameter("content");
                String linkImg = req.getParameter("linkImg");
                int state = 1;
                Product product = new Product(id, name, idCategory, idSubCategory, linkImg, price, sale, addDate,content, state);
                productAdminDAO.addProduct(product);
                resp.sendRedirect(req.getContextPath() + URLConstant.URL_ADMIN_PRODUCT);
                break;
            }
            case URLConstant.URL_ADMIN_PRODUCT_EDIT: {
                String id = req.getParameter("id");
                String name = req.getParameter("name");
                String idCategory = req.getParameter("idCategory");
                String idSubCategory = req.getParameter("idSubCategory");
                double price = Double.parseDouble(req.getParameter("price"));
                double sale = Double.parseDouble(req.getParameter("sale"));
                String addDate = simpleDateFormat.format(new Date());
                String content = req.getParameter("content");
                String linkImg = req.getParameter("linkImg");
                int state = 1;
                Product product = new Product(id, name, idCategory, idSubCategory, linkImg, price, sale, addDate,content, state);
                productAdminDAO.editProduct(product);
                resp.sendRedirect(req.getContextPath() + URLConstant.URL_ADMIN_PRODUCT);
                break;
            }
        }
    }
}
