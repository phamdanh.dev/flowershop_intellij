package com.bhd.controller;

import com.bhd.dao.ProductDAO;
import com.bhd.entity.Product;
import com.bhd.util.PathConstant;
import com.bhd.util.URLConstant;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(urlPatterns = URLConstant.URL_PRODUCT)
public class ProductController extends HttpServlet {
    ProductDAO productDAO;

    @Override
    public void init() throws ServletException {
        productDAO = new ProductDAO();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        String action = req.getServletPath();
//        switch (action){
//            case URLConstant.URL_PRODUCT:
//                ArrayList<Product> listChuDeHoaKhaiTruong = productDAO.getHKhaiTruong();
//                req.setAttribute("ChuDeHoaKhaiTruong", listChuDeHoaKhaiTruong);
//
//                ArrayList<Product> listChuDeHoaTinhyeu = productDAO.getHoaTinhYeu();
//                req.setAttribute("ChuDeHoaTinhyeu", listChuDeHoaTinhyeu);
//
//                ArrayList<Product> listChuDeHoaCuoi = productDAO.getHoaCuoi();
//                req.setAttribute("ChuDeHoaCuoi", listChuDeHoaCuoi);
//
//                ArrayList<Product> listChuDeHoaLe = productDAO.getHoaLe();
//                req.setAttribute("ChuDeHoaLe", listChuDeHoaLe);
//
//                ArrayList<Product> listChuDeHoaTrangTri = productDAO.getTrangTri();
//                req.setAttribute("ChuDeHoaTrangTri", listChuDeHoaTrangTri);
//
//                ArrayList<Product> listChuDeHoaTuoi = productDAO.getHoaTuoi();
//                req.setAttribute("ChuDeHoaTuoi", listChuDeHoaTuoi);
//                req.getRequestDispatcher(PathConstant.PRODUCT).forward(req, resp);
//        }
    }
}
