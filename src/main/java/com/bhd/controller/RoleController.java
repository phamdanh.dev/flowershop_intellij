package com.bhd.controller;

import com.bhd.dao.UserDAO;
import com.bhd.entity.Role;
import com.bhd.entity.User;
import com.bhd.util.PathConstant;
import com.bhd.util.URLConstant;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(urlPatterns = {URLConstant.URL_ADMIN_ROLE, URLConstant.URL_ADMIN_ROLE_ADD,
        URLConstant.URL_ADMIN_ROLE_EDIT, URLConstant.URL_ADMIN_ROLE_DELETE})
public class RoleController extends HttpServlet {

    UserDAO userDAO;

    @Override
    public void init() throws ServletException {
        userDAO = new UserDAO();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getServletPath();
        switch (action) {
            case URLConstant.URL_ADMIN_ROLE: {
                ArrayList<Role> roleList = userDAO.getAllRole();
                req.setAttribute("role", roleList);
                req.getRequestDispatcher(PathConstant.ADMIN_ROLE).forward(req, resp);
                break;
            }

            case URLConstant.URL_ADMIN_ROLE_ADD: {
                req.getRequestDispatcher(PathConstant.ADMIN_ROLE_ADD).forward(req, resp);
                break;
            }
            case URLConstant.URL_ADMIN_ROLE_EDIT: {
                int idRole = Integer.parseInt(req.getParameter("idRole"));
                Role role = userDAO.findRoleById(idRole);
                req.setAttribute("role", role);
                req.getRequestDispatcher(PathConstant.ADMIN_ROLE_EDIT).forward(req, resp);
                break;
            }
            case URLConstant.URL_ADMIN_ROLE_DELETE: {
                int idRole = Integer.parseInt(req.getParameter("idRole"));
                userDAO.deleteRoleById(idRole);
                resp.sendRedirect(req.getContextPath() + URLConstant.URL_ADMIN_ROLE);
                break;
            }

        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getServletPath();
        switch (action) {
            case URLConstant.URL_ADMIN_ROLE_ADD: {
                int idRole = Integer.parseInt(req.getParameter("idRole"));
                String nameRole = req.getParameter("nameRole");

                Role role = new Role();
                role.setIdRole(idRole);
                role.setNameRole(nameRole);

                boolean result = userDAO.addRole(role);

                if (!result) {
                    System.out.println("ADD ROLE ERROR");
                }

                resp.sendRedirect(req.getContextPath() + URLConstant.URL_ADMIN_ROLE);
                break;
            }

            case URLConstant.URL_ADMIN_ROLE_EDIT: {
                int idRole = Integer.parseInt(req.getParameter("idRole"));
                String nameRole = req.getParameter("nameRole");

                Role role = new Role();
                role.setIdRole(idRole);
                role.setNameRole(nameRole);

                boolean result = userDAO.editRole(role);

                if (!result) {
                    System.out.println("EDIT ROLE ERROR");
                }

                resp.sendRedirect(req.getContextPath() + URLConstant.URL_ADMIN_ROLE);

                break;
            }
        }
    }
}
