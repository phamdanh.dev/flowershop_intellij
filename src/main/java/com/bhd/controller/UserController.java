package com.bhd.controller;

import com.bhd.dao.UserDAO;
import com.bhd.entity.Role;
import com.bhd.entity.User;
import com.bhd.util.PathConstant;
import com.bhd.util.URLConstant;
import com.mysql.cj.Session;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(urlPatterns = {URLConstant.URL_USER_ACCOUNT, URLConstant.URL_USER_HISTORY,
        URLConstant.URL_REGISTER, URLConstant.URL_USER_ACCOUNT_EDIT,
        URLConstant.URL_ADMIN_ACCOUNT, URLConstant.URL_ADMIN_ACCOUNT_EDIT,
        URLConstant.URL_ADMIN_USER, URLConstant.URL_ADMIN_USER_ADD,
        URLConstant.URL_ADMIN_USER_EDIT, URLConstant.URL_ADMIN_USER_DELETE})
public class UserController extends HttpServlet {

    UserDAO userDAO;

    @Override
    public void init() throws ServletException {
        userDAO = new UserDAO();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getServletPath();
        switch (action) {
            case URLConstant.URL_REGISTER: {
                req.getRequestDispatcher(PathConstant.REGISTER).forward(req, resp);
                break;
            }
            case URLConstant.URL_USER_ACCOUNT: {
                HttpSession session = req.getSession();
                User user = (User) session.getAttribute("USER_LOGIN");
                req.setAttribute("user", user);
                req.getRequestDispatcher(PathConstant.USER).forward(req, resp);
                break;
            }
            case URLConstant.URL_USER_HISTORY: {
                req.getRequestDispatcher(PathConstant.USER_HISTORY).forward(req, resp);
                break;
            }
            case URLConstant.URL_ADMIN_ACCOUNT: {
                HttpSession session = req.getSession();
                User user = (User) session.getAttribute("ADMIN_LOGIN");
                req.setAttribute("user", user);
                req.getRequestDispatcher(PathConstant.ADMIN_ACCOUNT).forward(req, resp);
                break;
            }

            case  URLConstant.URL_ADMIN_USER: {
                ArrayList<User> userList = userDAO.getAllUser();
                req.setAttribute("user", userList);
                req.getRequestDispatcher(PathConstant.ADMIN_USER).forward(req, resp);
                break;
            }

            case  URLConstant.URL_ADMIN_USER_ADD: {
                ArrayList<Role> roleList = userDAO.getAllRole();
                req.setAttribute("role", roleList);
                req.getRequestDispatcher(PathConstant.ADMIN_USER_ADD).forward(req, resp);
                break;
            }

            case  URLConstant.URL_ADMIN_USER_EDIT: {
                ArrayList<Role> roleList = userDAO.getAllRole();
                req.setAttribute("role", roleList);
                int idUser = Integer.parseInt(req.getParameter("idUser"));
                User user = userDAO.findUserById(idUser);
                req.setAttribute("user", user);
                req.getRequestDispatcher(PathConstant.ADMIN_USER_EDIT).forward(req, resp);
                break;
            }

            case  URLConstant.URL_ADMIN_USER_DELETE: {
                int idUser = Integer.parseInt(req.getParameter("idUser"));
                userDAO.deleteUserById(idUser);
                resp.sendRedirect(req.getContextPath() + URLConstant.URL_ADMIN_USER);
                break;
            }

        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getServletPath();
        switch (action) {
            case URLConstant.URL_REGISTER: {
                String name = req.getParameter("name");
                String email = req.getParameter("email");
                String phone = req.getParameter("phone");
                String password = req.getParameter("password");
                String address = req.getParameter("address");

                User user = new User();
                user.setIdRole(1);
                user.setName(name);
                user.setEmail(email);
                user.setPhone(phone);
                user.setPassword(password);
                user.setAddress(address);

                boolean result = userDAO.addUser(user);

                if (!result) {
                    System.out.println("ADD USER ERROR");
                }

                resp.sendRedirect(req.getContextPath() + URLConstant.URL_LOGIN);

                break;
            }
            case URLConstant.URL_USER_ACCOUNT_EDIT: {
                HttpSession session = req.getSession();
                User user = (User) session.getAttribute("USER_LOGIN");
                int id = user.getId();
                int idRole = user.getIdRole();
                String name = req.getParameter("name");
                String email = req.getParameter("email");
                String phone = req.getParameter("phone");
                String password = req.getParameter("password");
                String address = req.getParameter("address");

                User userEdit = new User();
                userEdit.setId(id);
                userEdit.setIdRole(idRole);
                userEdit.setName(name);
                userEdit.setEmail(email);
                userEdit.setPhone(phone);
                userEdit.setPassword(password);
                userEdit.setAddress(address);

                User userEdited = userDAO.editUser(userEdit);
                session.setAttribute("USER_LOGIN", userEdited);
                resp.sendRedirect(req.getContextPath() + URLConstant.URL_USER_ACCOUNT);
                break;
            }

            case URLConstant.URL_ADMIN_ACCOUNT_EDIT: {
                HttpSession session = req.getSession();
                User user = (User) session.getAttribute("ADMIN_LOGIN");
                int id = user.getId();
                int idRole = user.getIdRole();
                String name = req.getParameter("name");
                String email = req.getParameter("email");
                String phone = req.getParameter("phone");
                String password = req.getParameter("password");
                String address = req.getParameter("address");

                User userEdit = new User();
                userEdit.setId(id);
                userEdit.setIdRole(idRole);
                userEdit.setName(name);
                userEdit.setEmail(email);
                userEdit.setPhone(phone);
                userEdit.setPassword(password);
                userEdit.setAddress(address);

                User userEdited = userDAO.editAdminAccount(userEdit);
                session.setAttribute("ADMIN_LOGIN", userEdited);
                resp.sendRedirect(req.getContextPath() + URLConstant.URL_ADMIN_ACCOUNT);
                break;
            }

            case URLConstant.URL_ADMIN_USER_ADD: {
                int idRole = Integer.parseInt(req.getParameter("idRole"));
                String name = req.getParameter("name");
                String email = req.getParameter("email");
                String phone = req.getParameter("phone");
                String password = req.getParameter("password");
                String address = req.getParameter("address");

                User user = new User();
                user.setIdRole(idRole);
                user.setName(name);
                user.setEmail(email);
                user.setPhone(phone);
                user.setPassword(password);
                user.setAddress(address);

                boolean result = userDAO.addUser(user);

                if (!result) {
                    System.out.println("ADD USER ERROR");
                }

                resp.sendRedirect(req.getContextPath() + URLConstant.URL_ADMIN_USER);

                break;
            }

            case URLConstant.URL_ADMIN_USER_EDIT: {
                int userId = Integer.parseInt(req.getParameter("idUser"));
                int idRole = Integer.parseInt(req.getParameter("idRole"));
                String name = req.getParameter("name");
                String email = req.getParameter("email");
                String phone = req.getParameter("phone");
                String password = req.getParameter("password");
                String address = req.getParameter("address");

                User user = new User();
                user.setId(userId);
                user.setIdRole(idRole);
                user.setName(name);
                user.setEmail(email);
                user.setPhone(phone);
                user.setPassword(password);
                user.setAddress(address);

                boolean result = userDAO.editUserByAdmin(user);

                if (!result) {
                    System.out.println("EDIT USER ERROR");
                }

                resp.sendRedirect(req.getContextPath() + URLConstant.URL_ADMIN_USER);

                break;
            }
        }
    }
}
