package com.bhd.dao;

import com.bhd.connection.JDBCConnection;
import com.bhd.entity.Category;
import com.bhd.entity.Role;
import com.bhd.entity.SubCategory;
import com.bhd.entity.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class CategoryDAO {

    //CATEGORY
    public static ArrayList<Category> getAllCategory() {
        String query = "SELECT * FROM Category";
        ArrayList<Category> categories = new ArrayList<Category>();

        try {
            Connection connection = JDBCConnection.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet rs = statement.executeQuery();

            while (rs.next()) {
                Category category = new Category();
                category.setIdCategory(rs.getString("idCategory"));
                category.setNameCategory(rs.getString("nameCategory"));
                category.setLinkCategory(rs.getString("linkCategory"));
                categories.add(category);
            }

            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return categories;
    }

    public static Category findCategoryById(String idCategory) {
        String query = "SELECT * FROM Category WHERE idCategory = ?";
        Category category = null;
        try {
            Connection connection = JDBCConnection.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, idCategory);
            System.out.println(statement);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                category = new Category();
                category.setIdCategory(rs.getString("idCategory"));
                category.setNameCategory(rs.getString("nameCategory"));
                category.setLinkCategory(rs.getString("linkCategory"));

            }

            connection.close();
        } catch (SQLException e) {
            System.out.println("KHÔNG TÌM THẤY CATEGORY VỚI id = " + idCategory);
            e.printStackTrace();
        }

        return category;
    }

    public static boolean deleteCategoryById(String idCategory) {
        String query = "DELETE FROM Category WHERE idCategory = ?";
        try {
            Connection connection = JDBCConnection.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, idCategory);
            statement.executeUpdate();
            connection.close();
            return true;
        } catch (SQLException e) {
            System.out.println("DELETE KHONG THANH CONG CATEGORY CategoryID = " + idCategory);
            e.printStackTrace();
            return false;
        }

    }

    public static boolean addCategory(Category category) {
        String query = "INSERT INTO Category (idCategory, nameCategory, linkCategory) VALUES (?, ?, ?)";
        try {
            Connection connection = JDBCConnection.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, category.getIdCategory());
            statement.setString(2, category.getNameCategory());
            statement.setString(3, category.getLinkCategory());
            statement.executeUpdate();
            connection.close();
            return true;
        } catch (Exception e) {
            System.out.println("ADD CATEGORY ERROR");
            e.printStackTrace();
            return false;
        }
    }

    public static boolean editCategory(Category category) {
        String query = "UPDATE Category SET nameCategory = ?, linkCategory = ? WHERE idCategory = ?";
        try {
            Connection connection = JDBCConnection.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, category.getNameCategory());
            statement.setString(2, category.getLinkCategory());
            statement.setString(3, category.getIdCategory());
            statement.executeUpdate();
            connection.close();
            return true;
        } catch (Exception e) {
            System.out.println("EDIT CATEGORY ERROR");
            e.printStackTrace();
            return false;
        }
    }

    //SUBCATEGORY
    public static ArrayList<SubCategory> getAllSubCategory() {
        String query = "SELECT * FROM SubCategory";
        ArrayList<SubCategory> subCategories = new ArrayList<SubCategory>();

        try {
            Connection connection = JDBCConnection.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet rs = statement.executeQuery();

            while (rs.next()) {
                SubCategory subCategory = new SubCategory();
                subCategory.setIdSubCategory((rs.getString("idSubCategory")));
                subCategory.setIdCategory(rs.getString("idCategory"));
                subCategory.setNameSubCategory(rs.getString("nameSubCategory"));
                subCategories.add(subCategory);
            }

            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return subCategories;
    }

    public static SubCategory findSubCategoryById(String idSubCategory) {
        String query = "SELECT * FROM SubCategory WHERE idSubCategory = ?";
        SubCategory subCategory = null;
        try {
            Connection connection = JDBCConnection.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, idSubCategory);
            System.out.println(statement);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                subCategory = new SubCategory();
                subCategory.setIdSubCategory(rs.getString("idSubCategory"));
                subCategory.setIdCategory(rs.getString("idCategory"));
                subCategory.setNameSubCategory(rs.getString("nameSubCategory"));

            }

            connection.close();
        } catch (SQLException e) {
            System.out.println("KHÔNG TÌM THẤY SUBCATEGORY VỚI id = " + idSubCategory);
            e.printStackTrace();
        }

        return subCategory;
    }

    public static boolean deleteSubCategoryById(String idSubCategory) {
        String query = "DELETE FROM SubCategory WHERE idSubCategory = ?";
        try {
            Connection connection = JDBCConnection.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, idSubCategory);
            statement.executeUpdate();
            connection.close();
            return true;
        } catch (SQLException e) {
            System.out.println("DELETE KHONG THANH CONG SUBCATEGORY SubCategoryID = " + idSubCategory);
            e.printStackTrace();
            return false;
        }

    }

    public static boolean addSubCategory(SubCategory subCategory) {
        String query = "INSERT INTO SubCategory (idSubCategory, idCategory, nameSubCategory) VALUES (?, ?, ?)";
        try {
            Connection connection = JDBCConnection.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, subCategory.getIdSubCategory());
            statement.setString(2, subCategory.getIdCategory());
            statement.setString(3, subCategory.getNameSubCategory());
            statement.executeUpdate();
            connection.close();
            return true;
        } catch (Exception e) {
            System.out.println("ADD SUBCATEGORY ERROR");
            e.printStackTrace();
            return false;
        }
    }

    public static boolean editSubCategory(SubCategory subCategory) {
        String query = "UPDATE SubCategory SET idCategory = ?, nameSubCategory = ? WHERE idSubCategory = ?";
        try {
            Connection connection = JDBCConnection.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, subCategory.getIdCategory());
            statement.setString(2, subCategory.getNameSubCategory());
            statement.setString(3, subCategory.getIdSubCategory());
            statement.executeUpdate();
            connection.close();
            return true;
        } catch (Exception e) {
            System.out.println("EDIT SUBCATEGORY ERROR");
            e.printStackTrace();
            return false;
        }
    }

    public static ArrayList<SubCategory> getSubCategoryByIdCategory(String idCategory) {
        String query = "SELECT * FROM SubCategory WHERE idCategory = ?";
        ArrayList<SubCategory> subCategories = new ArrayList<SubCategory>();

        try {
            Connection connection = JDBCConnection.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, idCategory);
            ResultSet rs = statement.executeQuery();

            while (rs.next()) {
                SubCategory subCategory = new SubCategory();
                subCategory.setIdSubCategory((rs.getString("idSubCategory")));
                subCategory.setIdCategory(rs.getString("idCategory"));
                subCategory.setNameSubCategory(rs.getString("nameSubCategory"));
                subCategories.add(subCategory);
            }

            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return subCategories;
    }
}
