package com.bhd.dao;

import com.bhd.connection.JDBCConnection;
import com.bhd.entity.Category;
import com.bhd.entity.Product;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class HomeDAO {

//    public ArrayList<Category> getChuDe() {
//        String query = "SELECT * FROM category ";
//        ArrayList<Category> listCategory = new ArrayList<>();
//        try {
//            Connection connection = JDBCConnection.getConnection();
//            PreparedStatement statement = connection.prepareStatement(query);
//            ResultSet rs = statement.executeQuery();
//
//            while (rs.next()) {
//                Category category = new Category();
//                category.setIdCategory(rs.getString("idCategory"));
//                category.setNameCategory(rs.getString("nameCategory"));
//                category.setLinkCategory(rs.getString("linkCategory"));
//
//                listCategory.add(category);
//
//
//            }
//
//        } catch (Exception e) {
//
//        }
//        return listCategory;
//    }
//
//    public ArrayList<Product> getNewProducts() {
//        String query = "SELECT * FROM product ORDER BY addDate DESC LIMIT 0, 4";
//        ArrayList<Product> listProduct = new ArrayList<>();
//        try {
//            Connection connection = JDBCConnection.getConnection();
//            PreparedStatement statement = connection.prepareStatement(query);
//            ResultSet rs = statement.executeQuery();
//
//            while (rs.next()) {
//                Product product = new Product();
//                product.setId(rs.getString("idProduct"));
//                product.setName(rs.getString("nameProduct"));
//                product.setIdCategory(rs.getInt("idCategory"));
//                product.setIdSubCategory(rs.getInt("subCategory"));
//                product.setImages(rs.getString("linkImg"));
//                product.setPrice(rs.getDouble("price"));
//                product.setSale(rs.getDouble("sale"));
//                product.setAddDate(rs.getString("addDate"));
//                listProduct.add(product);
//
//
//            }
//
//        } catch (Exception e) {
//
//        }
//        return listProduct;
//    }
//
//
//    public ArrayList<Product> getBestSellingProducts() {
//        String query = "SELECT * FROM product AS p JOIN oderItem AS o ON p.idProduct = o.idproduct ORDER BY o.quanlity DESC LIMIT 0, 4";
//        ArrayList<Product> listProduct = new ArrayList<>();
//        try {
//            Connection connection = JDBCConnection.getConnection();
//            PreparedStatement statement = connection.prepareStatement(query);
//            ResultSet rs = statement.executeQuery();
//
//            while (rs.next()) {
//                Product product = new Product();
//                product.setId(rs.getString("idProduct"));
//                product.setName(rs.getString("nameProduct"));
//                product.setIdCategory(rs.getInt("idCategory"));
//                product.setIdSubCategory(rs.getInt("subCategory"));
//                product.setImages(rs.getString("linkImg"));
//                product.setPrice(rs.getDouble("price"));
//                product.setSale(rs.getDouble("sale"));
//                product.setAddDate(rs.getString("addDate"));
//                listProduct.add(product);
//
//
//            }
//
//        } catch (Exception e) {
//
//        }
//        return listProduct;
//    }
//
//
//    public ArrayList<Product> getHoaKhaiTruong() {
//        String query = "SELECT * FROM product AS p JOIN category AS c ON p.idCategory = c.idCategory WHERE c.nameCategory = ? LIMIT 0, 4";
//        ArrayList<Product> listProduct = new ArrayList<>();
//        try {
//            Connection connection = JDBCConnection.getConnection();
//            PreparedStatement statement = connection.prepareStatement(query);
//            statement.setString(1, "Hoa Khai Trương");
//            ResultSet rs = statement.executeQuery();
//
//            while (rs.next()) {
//                Product product = new Product();
//                product.setId(rs.getString("idProduct"));
//                product.setName(rs.getString("nameProduct"));
//                product.setIdCategory(rs.getInt("idCategory"));
//                product.setIdSubCategory(rs.getInt("subCategory"));
//                product.setImages(rs.getString("linkImg"));
//                product.setPrice(rs.getDouble("price"));
//                product.setSale(rs.getDouble("sale"));
//                product.setAddDate(rs.getString("addDate"));
//                listProduct.add(product);
//
//
//            }
//
//        } catch (Exception e) {
//
//        }
//        return listProduct;
//    }
//
//    public ArrayList<Product> getHoaTinhYeu() {
//        String query = "SELECT * FROM product AS p JOIN category AS c ON p.idCategory = c.idCategory WHERE c.nameCategory = ? LIMIT 0, 4";
//        ArrayList<Product> listProduct = new ArrayList<>();
//        try {
//            Connection connection = JDBCConnection.getConnection();
//            PreparedStatement statement = connection.prepareStatement(query);
//            statement.setString(1, "Hoa Tinh Yeu");
//            ResultSet rs = statement.executeQuery();
//
//            while (rs.next()) {
//                Product product = new Product();
//                product.setId(rs.getString("idProduct"));
//                product.setName(rs.getString("nameProduct"));
//                product.setIdCategory(rs.getInt("idCategory"));
//                product.setIdSubCategory(rs.getInt("subCategory"));
//                product.setImages(rs.getString("linkImg"));
//                product.setPrice(rs.getDouble("price"));
//                product.setSale(rs.getDouble("sale"));
//                product.setAddDate(rs.getString("addDate"));
//                listProduct.add(product);
//
//
//            }
//
//        } catch (Exception e) {
//
//        }
//        return listProduct;
//    }
//
//    public ArrayList<Product> getHoaCuoi() {
//        String query = "SELECT * FROM product AS p JOIN category AS c ON p.idCategory = c.idCategory WHERE c.nameCategory = ? LIMIT 0, 4";
//        ArrayList<Product> listProduct = new ArrayList<>();
//        try {
//            Connection connection = JDBCConnection.getConnection();
//            PreparedStatement statement = connection.prepareStatement(query);
//            statement.setString(1, "Hoa Cưới");
//            ResultSet rs = statement.executeQuery();
//
//            while (rs.next()) {
//                Product product = new Product();
//                product.setId(rs.getString("idProduct"));
//                product.setName(rs.getString("nameProduct"));
//                product.setIdCategory(rs.getInt("idCategory"));
//                product.setIdSubCategory(rs.getInt("subCategory"));
//                product.setImages(rs.getString("linkImg"));
//                product.setPrice(rs.getDouble("price"));
//                product.setSale(rs.getDouble("sale"));
//                product.setAddDate(rs.getString("addDate"));
//                listProduct.add(product);
//
//
//            }
//
//        } catch (Exception e) {
//
//        }
//        return listProduct;
//    }
//
//    public ArrayList<Product> getHoaLe() {
//        String query = "SELECT * FROM product AS p JOIN category AS c ON p.idCategory = c.idCategory WHERE c.nameCategory = ? LIMIT 0, 4";
//        ArrayList<Product> listProduct = new ArrayList<>();
//        try {
//            Connection connection = JDBCConnection.getConnection();
//            PreparedStatement statement = connection.prepareStatement(query);
//            statement.setString(1, "Hoa Lễ");
//            ResultSet rs = statement.executeQuery();
//
//            while (rs.next()) {
//                Product product = new Product();
//                product.setId(rs.getString("idProduct"));
//                product.setName(rs.getString("nameProduct"));
//                product.setIdCategory(rs.getInt("idCategory"));
//                product.setIdSubCategory(rs.getInt("subCategory"));
//                product.setImages(rs.getString("linkImg"));
//                product.setPrice(rs.getDouble("price"));
//                product.setSale(rs.getDouble("sale"));
//                product.setAddDate(rs.getString("addDate"));
//                listProduct.add(product);
//
//
//            }
//
//        } catch (Exception e) {
//
//        }
//        return listProduct;
//    }
//
//    public ArrayList<Product> getHoaTrangTri() {
//        String query = "SELECT * FROM product AS p JOIN category AS c ON p.idCategory = c.idCategory WHERE c.nameCategory = ? LIMIT 0, 4";
//        ArrayList<Product> listProduct = new ArrayList<>();
//        try {
//            Connection connection = JDBCConnection.getConnection();
//            PreparedStatement statement = connection.prepareStatement(query);
//            statement.setString(1, "Hoa Trang Trí");
//            ResultSet rs = statement.executeQuery();
//
//            while (rs.next()) {
//                Product product = new Product();
//                product.setId(rs.getString("idProduct"));
//                product.setName(rs.getString("nameProduct"));
//                product.setIdCategory(rs.getInt("idCategory"));
//                product.setIdSubCategory(rs.getInt("subCategory"));
//                product.setImages(rs.getString("linkImg"));
//                product.setPrice(rs.getDouble("price"));
//                product.setSale(rs.getDouble("sale"));
//                product.setAddDate(rs.getString("addDate"));
//                listProduct.add(product);
//
//
//            }
//
//        } catch (Exception e) {
//
//        }
//        return listProduct;
//    }
//
//    public ArrayList<Product> getHoaTuoi() {
//        String query = "SELECT * FROM product AS p JOIN category AS c ON p.idCategory = c.idCategory WHERE c.nameCategory = ? LIMIT 0, 4";
//        ArrayList<Product> listProduct = new ArrayList<>();
//        try {
//            Connection connection = JDBCConnection.getConnection();
//            PreparedStatement statement = connection.prepareStatement(query);
//            statement.setString(1, "Hoa Tươi");
//            ResultSet rs = statement.executeQuery();
//
//            while (rs.next()) {
//                Product product = new Product();
//                product.setId(rs.getString("idProduct"));
//                product.setName(rs.getString("nameProduct"));
//                product.setIdCategory(rs.getInt("idCategory"));
//                product.setIdSubCategory(rs.getInt("subCategory"));
//                product.setImages(rs.getString("linkImg"));
//                product.setPrice(rs.getDouble("price"));
//                product.setSale(rs.getDouble("sale"));
//                product.setAddDate(rs.getString("addDate"));
//                listProduct.add(product);
//
//
//            }
//
//        } catch (Exception e) {
//
//        }
//        return listProduct;
//    }
}
