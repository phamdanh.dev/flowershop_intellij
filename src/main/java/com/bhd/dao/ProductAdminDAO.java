package com.bhd.dao;

import com.bhd.connection.JDBCConnection;
import com.bhd.dto.ProductDTO;
import com.bhd.entity.Product;
import com.bhd.entity.Role;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ProductAdminDAO {

    public static ProductDTO getProductDTOById(String id) {
        String query = "SELECT p.idProduct, p.name, c.nameCategory, s.nameSubCategory, i.linkImg, p.price, p.sale, p.addDate, p.content, p.active FROM Product AS p JOIN Category AS c ON p.idCategory = c.idCategory JOIN SubCategory AS s ON s.idSubCategory = p.idSubCategory JOIN Image as i ON p.idProduct  = i.idProduct WHERE p.idProduct = ?";
        ProductDTO productDTO = null;
        try {
            Connection connection = JDBCConnection.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, id);
            ResultSet rs = statement.executeQuery();

            while (rs.next()) {
                productDTO = new ProductDTO();
                productDTO.setId(rs.getString("idProduct"));
                productDTO.setName(rs.getString("name"));
                productDTO.setNameCategory(rs.getString("nameCategory"));
                productDTO.setNameSubCategory(rs.getString("nameSubCategory"));
                productDTO.setImages(rs.getString("linkImg"));
                productDTO.setPrice(rs.getDouble("price"));
                productDTO.setSale(rs.getDouble("sale"));
                productDTO.setAddDate(rs.getString("addDate"));
                productDTO.setContent(rs.getString("content"));
                productDTO.setNameState((rs.getInt("active") == 1 ? "Kích hoạt" : "Ẩn"));
            }

            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return productDTO;
    }

    public static ArrayList<ProductDTO> getAllProductDto() {
        String query = "SELECT p.idProduct, p.name, c.nameCategory, s.nameSubCategory, i.linkImg, p.price, p.sale, p.addDate, p.content, p.active FROM Product AS p JOIN Category AS c ON p.idCategory = c.idCategory JOIN SubCategory AS s ON s.idSubCategory = p.idSubCategory JOIN Image as i ON p.idProduct  = i.idProduct";
        ArrayList<ProductDTO> productDtoList = new ArrayList<ProductDTO>();

        try {
            Connection connection = JDBCConnection.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet rs = statement.executeQuery();

            while (rs.next()) {
                ProductDTO productDTO = new ProductDTO();
                productDTO.setId(rs.getString("idProduct"));
                productDTO.setName(rs.getString("name"));
                productDTO.setNameCategory(rs.getString("nameCategory"));
                productDTO.setNameSubCategory(rs.getString("nameSubCategory"));
                productDTO.setImages(rs.getString("linkImg"));
                productDTO.setPrice(rs.getDouble("price"));
                productDTO.setSale(rs.getDouble("sale"));
                productDTO.setAddDate(rs.getString("addDate"));
                productDTO.setContent(rs.getString("content"));
                productDTO.setNameState((rs.getInt("active") == 1 ? "Kích hoạt" : "Ẩn"));
                productDtoList.add(productDTO);
            }

            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return productDtoList;
    }

    public static boolean addProduct(Product product) {
        String query1 = "INSERT INTO Product (idProduct, idCategory, idSubCategory, name, price, sale, addDate, content, active) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        String query2 = "INSERT INTO image (idImage, idProduct, linkImg) VALUES (null , ?, ?)";
        try {
            Connection connection = JDBCConnection.getConnection();
            PreparedStatement statement1 = connection.prepareStatement(query1);
            statement1.setString(1, product.getId());
            statement1.setString(2, product.getIdCategory());
            statement1.setString(3, product.getIdSubCategory());
            statement1.setString(4, product.getName());
            statement1.setDouble(5, product.getPrice());
            statement1.setDouble(6, product.getSale());
            statement1.setString(7, product.getAddDate());
            statement1.setString(8, product.getContent());
            statement1.setInt(9, product.getState());
            statement1.executeUpdate();
            PreparedStatement statement2 = connection.prepareStatement(query2);
            statement2.setString(1, product.getId());
            statement2.setString(2, product.getImages());
            statement2.executeUpdate();
            connection.close();
            return true;
        } catch (Exception e) {
            System.out.println("ADD PRODUCT ERROR");
            e.printStackTrace();
            return false;
        }
    }

    public static boolean editProduct(Product product) {
        String query1 = "UPDATE Product SET idCategory = ?, idSubCategory = ?, name = ?, price = ?, sale = ?, content = ? WHERE idProduct = ?";
        String query2 = "UPDATE image SET linkImg = ? WHERE idProduct = ?";
        try {
            Connection connection = JDBCConnection.getConnection();
            PreparedStatement statement1 = connection.prepareStatement(query1);

            statement1.setString(1, product.getIdCategory());
            if (product.getIdSubCategory() == null) {
                statement1.setString(2, "null");
            } else {
                statement1.setString(2, product.getIdSubCategory());
            }
            statement1.setString(3, product.getName());
            statement1.setDouble(4, product.getPrice());
            statement1.setDouble(5, product.getSale());
            statement1.setString(6, product.getContent());
            statement1.setString(7, product.getId());
            statement1.executeUpdate();
            PreparedStatement statement2 = connection.prepareStatement(query2);
            statement2.setString(1, product.getImages());
            statement2.setString(2, product.getId());
            statement2.executeUpdate();
            connection.close();
            return true;
        } catch (Exception e) {
            System.out.println("EDIT PRODUCT ERROR");
            e.printStackTrace();
            return false;
        }
    }


    public static boolean deleteProductById(String idProduct) {
        String query = "DELETE FROM Product WHERE idProduct = ?";
        try {
            Connection connection = JDBCConnection.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, idProduct);
            statement.executeUpdate();
            connection.close();
            return true;
        } catch (SQLException e) {
            System.out.println("KHÔNG XOÁ ĐƯỢC PRODUCT VỚI idProduct = " + idProduct);
            e.printStackTrace();
            return false;
        }

    }

    public static boolean deleteImageByIdProduct(String idProduct) {
        String query = "DELETE FROM Image WHERE idProduct = ?";
        try {
            Connection connection = JDBCConnection.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, idProduct);
            statement.executeUpdate();
            connection.close();
            return true;
        } catch (SQLException e) {
            System.out.println("KHÔNG XOÁ ĐƯỢC IMAGE VỚI idProduct = " + idProduct);
            e.printStackTrace();
            return false;
        }

    }

}
