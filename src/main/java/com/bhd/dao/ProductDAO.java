package com.bhd.dao;

import com.bhd.connection.JDBCConnection;
import com.bhd.dto.ProductDTO;
import com.bhd.entity.Product;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ProductDAO {
//    public ArrayList<Product> getHKhaiTruong() {
//        String query = "SELECT * FROM product AS p JOIN category AS c ON p.idCategory = c.idCategory WHERE c.nameCategory = ?";
//        ArrayList<Product> listProduct = new ArrayList<>();
//        try {
//            Connection connection = JDBCConnection.getConnection();
//            PreparedStatement statement = connection.prepareStatement(query);
//            statement.setString(1, "Hoa Khai Trương");
//            ResultSet rs = statement.executeQuery();
//
//            while (rs.next()) {
//                Product product = new Product();
//                product.setId(rs.getString("idProduct"));
//                product.setName(rs.getString("nameProduct"));
//                product.setIdCategory(rs.getInt("idCategory"));
//                product.setIdSubCategory(rs.getInt("subCategory"));
//                product.setImages(rs.getString("linkImg"));
//                product.setPrice(rs.getDouble("price"));
//                product.setSale(rs.getDouble("sale"));
//                product.setAddDate(rs.getString("addDate"));
//                listProduct.add(product);
//
//
//            }
//
//        } catch (Exception e) {
//
//        }
//        return listProduct;
//    }

    public static ArrayList<ProductDTO> getProductDTOsByIdCategory(String idCategory) {
        String query = "SELECT p.idProduct, p.name, c.nameCategory, s.nameSubCategory, i.linkImg, p.price, p.sale, p.addDate, p.content, p.active FROM Product AS p JOIN Category AS c ON p.idCategory = c.idCategory JOIN SubCategory AS s ON s.idSubCategory = p.idSubCategory JOIN Image as i ON p.idProduct  = i.idProduct WHERE  p.idCategory = ? AND p.active = 1";
        ArrayList<ProductDTO> productDtoList = new ArrayList<ProductDTO>();

        try {
            Connection connection = JDBCConnection.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, idCategory);
            ResultSet rs = statement.executeQuery();

            while (rs.next()) {
                ProductDTO productDTO = new ProductDTO();
                productDTO.setId(rs.getString("idProduct"));
                productDTO.setName(rs.getString("name"));
                productDTO.setNameCategory(rs.getString("nameCategory"));
                productDTO.setNameSubCategory(rs.getString("nameSubCategory"));
                productDTO.setImages(rs.getString("linkImg"));
                productDTO.setPrice(rs.getDouble("price"));
                productDTO.setSale(rs.getDouble("sale"));
                productDTO.setAddDate(rs.getString("addDate"));
                productDTO.setContent(rs.getString("content"));
                productDTO.setNameState((rs.getInt("active") == 1 ? "Kích hoạt" : "Ẩn"));
                productDtoList.add(productDTO);
            }

            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return productDtoList;
    }

    public static ArrayList<ProductDTO> get4ProductDTOsByNameCategory(String nameCategory) {
        String query = "SELECT p.idProduct, p.name, c.nameCategory, s.nameSubCategory, i.linkImg, p.price, p.sale, p.addDate, p.content, p.active FROM Product AS p JOIN Category AS c ON p.idCategory = c.idCategory JOIN SubCategory AS s ON s.idSubCategory = p.idSubCategory JOIN Image as i ON p.idProduct  = i.idProduct WHERE  c.nameCategory LIKE ? AND p.active = 1 LIMIT 4";
        ArrayList<ProductDTO> productDtoList = new ArrayList<ProductDTO>();

        try {
            Connection connection = JDBCConnection.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, "%" + nameCategory + "%");
            ResultSet rs = statement.executeQuery();

            while (rs.next()) {
                ProductDTO productDTO = new ProductDTO();
                productDTO.setId(rs.getString("idProduct"));
                productDTO.setName(rs.getString("name"));
                productDTO.setNameCategory(rs.getString("nameCategory"));
                productDTO.setNameSubCategory(rs.getString("nameSubCategory"));
                productDTO.setImages(rs.getString("linkImg"));
                productDTO.setPrice(rs.getDouble("price"));
                productDTO.setSale(rs.getDouble("sale"));
                productDTO.setAddDate(rs.getString("addDate"));
                productDTO.setContent(rs.getString("content"));
                productDTO.setNameState((rs.getInt("active") == 1 ? "Kích hoạt" : "Ẩn"));
                productDtoList.add(productDTO);
            }

            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return productDtoList;
    }

}
