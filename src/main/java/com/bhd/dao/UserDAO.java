package com.bhd.dao;

import com.bhd.connection.JDBCConnection;
import com.bhd.entity.Role;
import com.bhd.entity.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class UserDAO {

    public static ArrayList<User> findUserByName(String name) {
        String query = "SELECT * FROM user WHERE name = ?";
        ArrayList<User> userList = new ArrayList<User>();

        try {
            Connection connection = JDBCConnection.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, name);
            System.out.println(statement);
            ResultSet rs = statement.executeQuery();

            while (rs.next()) {
                User user = new User();
                user.setId(rs.getInt("idUser"));
                user.setName(rs.getString("name"));
                user.setIdRole(rs.getInt("idRole"));
                user.setEmail(rs.getString("email"));
                user.setPhone(rs.getString("phone"));
                user.setPassword(rs.getString("password"));
                user.setAddress(rs.getString("address"));
                userList.add(user);
            }

            connection.close();
        } catch (SQLException e) {
            System.out.println("KHÔNG TÌM THẤY USER VỚI NAME = " + name);
            e.printStackTrace();
        }

        return userList;
    }

    public static User findUserById(int idUser) {
        String query = "SELECT * FROM user WHERE idUser = ?";
        User user = null;
        try {
            Connection connection = JDBCConnection.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, idUser);
            System.out.println(statement);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                user = new User();
                user.setId(rs.getInt("idUser"));
                user.setName(rs.getString("name"));
                user.setIdRole(rs.getInt("idRole"));
                user.setEmail(rs.getString("email"));
                user.setPhone(rs.getString("phone"));
                user.setPassword(rs.getString("password"));
                user.setAddress(rs.getString("address"));

            }

            connection.close();
        } catch (SQLException e) {
            System.out.println("KHÔNG TÌM THẤY USER VỚI UserID = " + idUser);
            e.printStackTrace();
        }

        return user;
    }

    public static User findUserByEmail(String email) {
        String query = "SELECT * FROM user WHERE email = ?";
        User user = null;

        try {
            Connection connection = JDBCConnection.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, email);
            System.out.println(statement);
            ResultSet rs = statement.executeQuery();

            while (rs.next()) {
                user = new User();
                user.setId(rs.getInt("idUser"));
                user.setName(rs.getString("name"));
                user.setIdRole(rs.getInt("idRole"));
                user.setEmail(rs.getString("email"));
                user.setPhone(rs.getString("phone"));
                user.setPassword(rs.getString("password"));
                user.setAddress(rs.getString("address"));
            }

            connection.close();
        } catch (Exception e) {
            System.out.println("KHÔNG TÌM THẤY USER VỚI EMAIL = " + email);
            e.printStackTrace();
        }

        return user;
    }

    public static User findUserByPhone(String phone) {
        String query = "SELECT * FROM user WHERE phone = ? AND idRole = 1";
        User user = null;

        try {
            Connection connection = JDBCConnection.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, phone);
            System.out.println(statement);
            ResultSet rs = statement.executeQuery();

            while (rs.next()) {
                user = new User();
                user.setId(rs.getInt("idUser"));
                user.setName(rs.getString("name"));
                user.setIdRole(rs.getInt("idRole"));
                user.setEmail(rs.getString("email"));
                user.setPhone(rs.getString("phone"));
                user.setPassword(rs.getString("password"));
                user.setAddress(rs.getString("address"));
            }

            connection.close();
        } catch (Exception e) {
            System.out.println("KHÔNG TÌM THẤY USER VỚI PHONE = " + phone);
            e.printStackTrace();
        }

        return user;
    }

    public static User findAdminByPhone(String phone) {
        String query = "SELECT * FROM user WHERE phone = ? AND idRole = 0";
        User user = null;

        try {
            Connection connection = JDBCConnection.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, phone);
            System.out.println(statement);
            ResultSet rs = statement.executeQuery();

            while (rs.next()) {
                user = new User();
                user.setId(rs.getInt("idUser"));
                user.setName(rs.getString("name"));
                user.setIdRole(rs.getInt("idRole"));
                user.setEmail(rs.getString("email"));
                user.setPhone(rs.getString("phone"));
                user.setPassword(rs.getString("password"));
                user.setAddress(rs.getString("address"));
            }

            connection.close();
        } catch (Exception e) {
            System.out.println("KHÔNG TÌM THẤY USER VỚI PHONE = " + phone);
            e.printStackTrace();
        }

        return user;
    }

    public static User editUser(User userEdit) {
        String query = "UPDATE User SET idRole =?, name = ?, email = ?, phone = ?, password = ?, address = ? WHERE idUser = ?";
        User user = null;
        try {
            Connection connection = JDBCConnection.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, userEdit.getIdRole());
            statement.setString(2, userEdit.getName());
            statement.setString(3, userEdit.getEmail());
            statement.setString(4, userEdit.getPhone());
            statement.setString(5, userEdit.getPassword());
            statement.setString(6, userEdit.getAddress());
            statement.setInt(7, userEdit.getId());
            System.out.println(statement);
            statement.executeUpdate();

            user = findUserByPhone(userEdit.getPhone());

            connection.close();
        } catch (Exception e) {
            System.out.println("EDIT USER ERROR");
            e.printStackTrace();
        }

        return user;
    }

    public static boolean editUserByAdmin(User userEdit) {
        String query = "UPDATE User SET idRole =?, name = ?, email = ?, phone = ?, password = ?, address = ? WHERE idUser = ?";
        try {
            Connection connection = JDBCConnection.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, userEdit.getIdRole());
            statement.setString(2, userEdit.getName());
            statement.setString(3, userEdit.getEmail());
            statement.setString(4, userEdit.getPhone());
            statement.setString(5, userEdit.getPassword());
            statement.setString(6, userEdit.getAddress());
            statement.setInt(7, userEdit.getId());
            System.out.println(statement);
            statement.executeUpdate();

            connection.close();
            return true;
        } catch (Exception e) {
            System.out.println("EDIT USER ERROR");
            e.printStackTrace();
            return false;
        }

    }

    public static boolean addUser(User user) {
        String query = "INSERT INTO User (idRole, name, email, phone, password, address) VALUES (?, ?, ?, ?, ?, ?)";
        try {
            Connection connection = JDBCConnection.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, user.getIdRole());
            statement.setString(2, user.getName());
            statement.setString(3, user.getEmail());
            statement.setString(4, user.getPhone());
            statement.setString(5, user.getPassword());
            statement.setString(6, user.getAddress());
            System.out.println(statement);
            statement.executeUpdate();
            connection.close();
            return true;
        } catch (Exception e) {
            System.out.println("ADD USER ERROR");
            e.printStackTrace();
            return false;
        }

    }

    public static User editAdminAccount(User userEdit) {
        String query = "UPDATE User SET idRole =?, name = ?, email = ?, phone = ?, password = ?, address = ? WHERE idUser = ?";
        User user = null;
        try {
            Connection connection = JDBCConnection.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, userEdit.getIdRole());
            statement.setString(2, userEdit.getName());
            statement.setString(3, userEdit.getEmail());
            statement.setString(4, userEdit.getPhone());
            statement.setString(5, userEdit.getPassword());
            statement.setString(6, userEdit.getAddress());
            statement.setInt(7, userEdit.getId());
            System.out.println(statement);
            statement.executeUpdate();

            user = findAdminByPhone(userEdit.getPhone());

            connection.close();
        } catch (Exception e) {
            System.out.println("EDIT ADMIN ACCOUNT ERROR");
            e.printStackTrace();
        }

        return user;
    }

    public static boolean deleteUserById(int idUser) {
        String query = "DELETE FROM User WHERE idUser = ?";
        try {
            Connection connection = JDBCConnection.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, idUser);
            statement.executeUpdate();
            connection.close();
            return true;
        } catch (SQLException e) {
            System.out.println("KHÔNG TÌM THẤY USER VỚI UserID = " + idUser);
            e.printStackTrace();
            return false;
        }

    }

    public static boolean deleteRoleById(int idRole) {
        String query = "DELETE FROM Role WHERE idRole = ?";
        try {
            Connection connection = JDBCConnection.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, idRole);
            statement.executeUpdate();
            connection.close();
            return true;
        } catch (SQLException e) {
            System.out.println("KHÔNG TÌM THẤY ROLE VỚI idRole = " + idRole);
            e.printStackTrace();
            return false;
        }

    }

    public static ArrayList<Role> getAllRole() {
        String query = "SELECT * FROM Role";
        ArrayList<Role> roleList = new ArrayList<Role>();

        try {
            Connection connection = JDBCConnection.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet rs = statement.executeQuery();

            while (rs.next()) {
                Role role = new Role();
                role.setIdRole(rs.getInt("idRole"));
                role.setNameRole(rs.getString("nameRole"));
                roleList.add(role);
            }

            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return roleList;
    }

    public static boolean addRole(Role role) {
        String query = "INSERT INTO Role (idRole, nameRole) VALUES (?, ?)";
        try {
            Connection connection = JDBCConnection.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, role.getIdRole());
            statement.setString(2, role.getNameRole());
            statement.executeUpdate();
            connection.close();
            return true;
        } catch (Exception e) {
            System.out.println("ADD ROLE ERROR");
            e.printStackTrace();
            return false;
        }
    }

    public static boolean editRole(Role role) {
        String query = "UPDATE Role SET nameRole = ? WHERE idRole = ?";
        try {
            Connection connection = JDBCConnection.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, role.getNameRole());
            statement.setInt(2, role.getIdRole());
            statement.executeUpdate();
            connection.close();
            return true;
        } catch (Exception e) {
            System.out.println("EDIT ROLE ERROR");
            e.printStackTrace();
            return false;
        }
    }

    public static Role findRoleById(int idRole) {
        String query = "SELECT * FROM Role WHERE idRole = ?";
        Role role = null;

        try {
            Connection connection = JDBCConnection.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, idRole);
            ResultSet rs = statement.executeQuery();

            while (rs.next()) {
                role = new Role();
                role.setIdRole(rs.getInt("idRole"));
                role.setNameRole(rs.getString("nameRole"));
            }

            connection.close();
        } catch (Exception e) {
            System.out.println("KHÔNG TÌM THẤY ROLE VỚI PHONE = " + idRole);
            e.printStackTrace();
        }

        return role;
    }

    public static ArrayList<User> getAllUser() {
        String query = "SELECT * FROM User";
        ArrayList<User> userList = new ArrayList<User>();

        try {
            Connection connection = JDBCConnection.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet rs = statement.executeQuery();

            while (rs.next()) {
                User user = new User();
                user.setId(rs.getInt("idUser"));
                user.setName(rs.getString("name"));
                user.setIdRole(rs.getInt("idRole"));
                user.setEmail(rs.getString("email"));
                user.setPhone(rs.getString("phone"));
                user.setPassword(rs.getString("password"));
                user.setAddress(rs.getString("address"));
                userList.add(user);
            }

            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return userList;
    }

}
