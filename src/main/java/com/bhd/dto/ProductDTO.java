package com.bhd.dto;

public class ProductDTO {

    private String id;
    private String name;
    private String nameCategory;
    private String nameSubCategory;
    private String images;
    private double price;
    private double sale;
    private String addDate;
    private String content;
    private String nameState;

    public ProductDTO() {

    }

    public ProductDTO(String id, String name, String nameCategory, String nameSubCategory, String images, double price, double sale, String addDate, String content, String nameState) {
        this.id = id;
        this.name = name;
        this.nameCategory = nameCategory;
        this.nameSubCategory = nameSubCategory;
        this.images = images;
        this.price = price;
        this.sale = sale;
        this.addDate = addDate;
        this.content = content;
        this.nameState = nameState;

    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getNameCategory() {
        return nameCategory;
    }

    public String getNameSubCategory() {
        return nameSubCategory;
    }

    public String getImages() {
        return images;
    }

    public double getPrice() {
        return price;
    }

    public double getSale() {
        return sale;
    }

    public String getAddDate() {
        return addDate;
    }

    public String getContent() {
        return content;
    }

    public String getNameState() {
        return nameState;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNameCategory(String nameCategory) {
        this.nameCategory = nameCategory;
    }

    public void setNameSubCategory(String nameSubCategory) {
        this.nameSubCategory = nameSubCategory;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setSale(double sale) {
        this.sale = sale;
    }

    public void setAddDate(String addDate) {
        this.addDate = addDate;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setNameState(String nameState) {
        this.nameState = nameState;
    }
}
