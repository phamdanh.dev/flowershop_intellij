package com.bhd.entity;

public class Category {
    private String idCategory;
    private String nameCategory;
    private String linkCategory;

    public Category(){

    }

    public Category(String idCategory, String nameCategory, String linkCategory) {
        this.idCategory = idCategory;
        this.nameCategory = nameCategory;
        this.linkCategory = linkCategory;
    }

    public String getNameCategory() {
        return nameCategory;
    }

    public String getLinkCategory() {
        return linkCategory;
    }

    public String getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(String idCategory) {
        this.idCategory = idCategory;
    }

    public void setNameCategory(String nameCategory) {
        this.nameCategory = nameCategory;
    }

    public void setLinkCategory(String linkCategory) {
        this.linkCategory = linkCategory;
    }
}
