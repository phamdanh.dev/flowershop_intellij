package com.bhd.entity;

import java.util.Date;

public class Product {
    private String id;
    private String name;
    private String idCategory;
    private String idSubCategory;
    private String images;
    private double price;
    private double sale;
    private String addDate;
    private String content;
    private int state;

    public Product(){

    }

    public Product(String id, String name, String idCategory, String idSubCategory, String images, double price, double sale, String addDate, String content, int state) {
        this.id = id;
        this.name = name;
        this.idCategory = idCategory;
        this.idSubCategory = idSubCategory;
        this.images = images;
        this.price = price;
        this.sale = sale;
        this.addDate = addDate;
        this.content = content;
        this.state = state;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getIdCategory() {
        return idCategory;
    }

    public String getIdSubCategory() {
        return idSubCategory;
    }

    public String getImages() {
        return images;
    }

    public double getPrice() {
        return price;
    }

    public double getSale() {
        return sale;
    }

    public String getAddDate() {
        return addDate;
    }

    public String getContent() {
        return content;
    }

    public int getState() {
        return state;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setIdCategory(String idCategory) {
        this.idCategory = idCategory;
    }

    public void setIdSubCategory(String idSubCategory) {
        this.idSubCategory = idSubCategory;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setSale(double sale) {
        this.sale = sale;
    }

    public void setAddDate(String addDate) {
        this.addDate = addDate;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setState(int state) {
        this.state = state;
    }
}
