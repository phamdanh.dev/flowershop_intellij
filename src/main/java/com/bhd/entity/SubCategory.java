package com.bhd.entity;

public class SubCategory {

    private String idSubCategory;
    private String idCategory;
    private String nameSubCategory;

    public SubCategory() {

    }

    public SubCategory(String idSubCategory, String idCategory, String nameSubCategory) {
        this.idSubCategory = idSubCategory;
        this.idCategory = idCategory;
        this.nameSubCategory = nameSubCategory;
    }

    public String getIdSubCategory() {
        return idSubCategory;
    }

    public String getIdCategory() {
        return idCategory;
    }

    public String getNameSubCategory() {
        return nameSubCategory;
    }

    public void setIdSubCategory(String idSubCategory) {
        this.idSubCategory = idSubCategory;
    }

    public void setIdCategory(String idCategory) {
        this.idCategory = idCategory;
    }

    public void setNameSubCategory(String nameSubCategory) {
        this.nameSubCategory = nameSubCategory;
    }
}
