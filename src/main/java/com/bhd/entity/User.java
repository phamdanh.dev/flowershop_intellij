package com.bhd.entity;

public class User {

    private int id;
    private String name;
    private int idRole;
    private String email;
    private String phone;
    private String password;
    private String address;

    public User() {

    }

    public User(int id, String name, int idRole, String email, String phone, String password, String address) {
        this.id = id;
        this.name = name;
        this.idRole = idRole;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getIdRole() {
        return idRole;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getPassword() {
        return password;
    }

    public String getAddress() {
        return address;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setIdRole(int idRole) {
        this.idRole = idRole;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setId(int id) {
        this.id = id;
    }
}
