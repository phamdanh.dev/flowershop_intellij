package com.bhd.filter;

import com.bhd.util.URLConstant;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(urlPatterns = "/admin/*")
public class AdminFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        String action = req.getServletPath();
        if (action.equals(URLConstant.URL_ADMIN_LOGIN)) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }
        HttpSession session = req.getSession();
        if (session.getAttribute("ADMIN_LOGIN") == null) {
            resp.sendRedirect(req.getContextPath() + URLConstant.URL_ADMIN_LOGIN);
            return;
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
