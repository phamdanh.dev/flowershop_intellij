package com.bhd.util;

public class PathConstant {

    //USER
    public static final String LOGIN = "/views/jsp/user/login.jsp";
    public static final String REGISTER  = "/views/jsp/user/register.jsp";
    public static final String HOME = "/views/jsp/user/home.jsp";
    public static final String USER = "/views/jsp/user/user.jsp";
    public static final String USER_HISTORY = "/views/jsp/user/userHistory.jsp";
    public static final String PRODUCT = "/views/jsp/user/product.jsp";

    //ADMIN
    public static final String ADMIN_LOGIN = "/views/jsp/admin/login.jsp";
    public static final String ADMIN_DASHBOARD = "/views/jsp/admin/dashboard.jsp";
    public static final String ADMIN_ACCOUNT = "/views/jsp/admin/account.jsp";
    public static final String ADMIN_USER = "/views/jsp/admin/user.jsp";
    public static final String ADMIN_USER_ADD = "/views/jsp/admin/addUser.jsp";
    public static final String ADMIN_USER_DETAILS = "/views/jsp/admin/userDetail.jsp";
    public static final String ADMIN_USER_EDIT = "/views/jsp/admin/editUser.jsp";
    public static final String ADMIN_ROLE = "/views/jsp/admin/role.jsp";
    public static final String ADMIN_ROLE_ADD = "/views/jsp/admin/addRole.jsp";
    public static final String ADMIN_ROLE_EDIT = "/views/jsp/admin/editRole.jsp";
    public static final String ADMIN_CATEGORY = "/views/jsp/admin/category.jsp";
    public static final String ADMIN_CATEGORY_ADD = "/views/jsp/admin/addCategory.jsp";
    public static final String ADMIN_CATEGORY_EDIT = "/views/jsp/admin/editCategory.jsp";
    public static final String ADMIN_SUBCATEGORY = "/views/jsp/admin/subCategory.jsp";
    public static final String ADMIN_SUBCATEGORY_ADD = "/views/jsp/admin/addSubCategory.jsp";
    public static final String ADMIN_SUBCATEGORY_EDIT = "/views/jsp/admin/editSubCategory.jsp";
    public static final String ADMIN_PRODUCT = "/views/jsp/admin/product.jsp";
    public static final String ADMIN_PRODUCT_ADD = "/views/jsp/admin/addProduct.jsp";
    public static final String ADMIN_PRODUCT_EDIT = "/views/jsp/admin/editProduct.jsp";
}
