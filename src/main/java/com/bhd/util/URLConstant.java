package com.bhd.util;

public class URLConstant {
    //AJAX
    public static final String AJAX_SUBCATEGORY = "/selectSubCategory";


    //USER
    public static final String URL_LOGIN = "/login";
    public static final String URL_LOGOUT = "/logout";
    public static final String URL_REGISTER = "/register";
    public static final String URL_HOME = "/home";
    public static final String URL_PRODUCT = "/product";
    public static final String URL_PRODUCT_CATEGORY = "/product/idCategory=?";
    public static final String URL_USER_HISTORY = "/history";
    public static final String URL_USER_ACCOUNT = "/account";
    public static final String URL_USER_ACCOUNT_EDIT = "/account/edit";
    public static final String URL_USER_PURCHASE = "/purchase";

    //ADMIN ====================================================
    public static final String URL_ADMIN_LOGIN = "/admin/login";
    public static final String URL_ADMIN_LOGOUT = "/admin/logout";
    public static final String URL_ADMIN_DASHBOARD = "/admin/dashboard";
    //Account
    public static final String URL_ADMIN_ACCOUNT = "/admin/account";
    public static final String URL_ADMIN_ACCOUNT_EDIT = "/admin/account/edit";
    //User
    public static final String URL_ADMIN_USER = "/admin/user";
    public static final String URL_ADMIN_USER_ADD = "/admin/user/add";
    public static final String URL_ADMIN_USER_DETAILS = "/admin/user/details";
    public static final String URL_ADMIN_USER_EDIT = "/admin/user/edit";
    public static final String URL_ADMIN_USER_DELETE = "/admin/user/delete";
    //Role
    public static final String URL_ADMIN_ROLE = "/admin/role";
    public static final String URL_ADMIN_ROLE_ADD = "/admin/role/add";
    public static final String URL_ADMIN_ROLE_EDIT = "/admin/role/edit";
    public static final String URL_ADMIN_ROLE_DELETE = "/admin/role/delete";
    //Category and SubCategory
    public static final String URL_ADMIN_CATEGORY = "/admin/category";
    public static final String URL_ADMIN_CATEGORY_ADD = "/admin/category/add";
    public static final String URL_ADMIN_CATEGORY_EDIT = "/admin/category/edit";
    public static final String URL_ADMIN_CATEGORY_DELETE = "/admin/category/delete";
    public static final String URL_ADMIN_SUBCATEGORY = "/admin/subcategory";
    public static final String URL_ADMIN_SUBCATEGORY_ADD = "/admin/subcategory/add";
    public static final String URL_ADMIN_SUBCATEGORY_EDIT = "/admin/subcategory/edit";
    public static final String URL_ADMIN_SUBCATEGORY_DELETE = "/admin/subcategory/delete";
    //Product
    public static final String URL_ADMIN_PRODUCT = "/admin/product";
    public static final String URL_ADMIN_PRODUCT_ADD = "/admin/product/add";
    public static final String URL_ADMIN_PRODUCT_EDIT = "/admin/product/edit";
    public static final String URL_ADMIN_PRODUCT_DELETE = "/admin/product/delete";
    public static final String URL_ADMIN_PRODUCT_CHANGESTATE = "/admin/product/changeState";
}
