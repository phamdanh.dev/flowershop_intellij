<%--
  Created by IntelliJ IDEA.
  User: Probook
  Date: 11-Dec-20
  Time: 1:14 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.bhd.util.URLConstant"%>
<html>
<head>
    <title>Thêm Tài Khoản</title>
    <link rel="stylesheet" href="/views/css/admin/bootstrap.min.css">
    <link rel="stylesheet" href="/views/css/admin/admin-common.css">
    <link rel="stylesheet" href="/views/css/admin/AddUser.css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/views/assets/myfonts/myfonts.css">
</head>
<body>
<section class="my-container">
    <%@include file="include/menu.jsp"%>
    <div class="dashboard">
        <%@include file="include/header.jsp"%>
        <div class="admin-header">
            <h1>Thêm Người Dùng</h1>
        </div>
        <!-- CONTENT-CHANGE -->
        <div class="content">
            <form class="mx-5" action='<c:url value='${ URLConstant.URL_ADMIN_ACCOUNT_EDIT }'></c:url>' method="POST">
                <div class="form-group">
                    <label>ID</label>
                    <input value='${user.id}' disabled>
                </div>
                <div class="form-group">
                    <label>Tên</label>
                    <input type="text" class="u-input" disabled value='${user.name}' name="name">
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input type="email" class="u-input" disabled value='${user.email}' name="email">
                </div>
                <div class="form-group">
                    <label>Số Điện Thoại</label>
                    <input type="text" class="u-input" disabled value='${user.phone}' name="phone">
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input type="text" class="u-input" disabled value='${user.password}' name="password">
                </div>
                <div class="form-group">
                    <label>Địa Chỉ</label>
                    <input type="text" class="u-input" disabled value='${user.address}' name="address">
                </div>
                <button type="button" id="edit-info" class="btn btn-primary">Sửa thông tin</button>
                <a id="reset-info" class="btn btn-danger" href="" style="display: none">Huỷ</a>
                <button id="save-info" type="submit" class="btn btn-primary" style="display: none">Submit</button>
            </form>

            <!-- END CONTENT CHANGE -->
        </div>
    </div>

</section>

<script>
    document.getElementById('edit-info').onclick = () => {
        document.getElementById('edit-info').style.display = 'none';
        document.getElementById('reset-info').style.display = 'inline-block';
        document.getElementById('save-info').style.display = 'inline-block';

        let inputArr = document.querySelectorAll('.u-input');
        for (let i = 0; i < inputArr.length; i++) {
            inputArr[i].style.backgroundColor = '#cccccc1c';
            inputArr[i].disabled = false;
        }
    }
</script>


<!-- SCRIPT -->
<script src="/views/js/jquery-3.2.1.slim.min.js"></script>
<script src="/views/js/popper.min.js"></script>
<script src="/views/js/bootstrap.min.js"></script>




</body>
</html>
