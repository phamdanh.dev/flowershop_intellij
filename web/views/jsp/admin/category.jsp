<%--
  Created by IntelliJ IDEA.
  User: Probook
  Date: 11-Dec-20
  Time: 1:22 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.bhd.util.URLConstant"%>
<html>
<head>
    <title>Danh Mục</title>
    <link rel="stylesheet" href="/views/css/admin/bootstrap.min.css">
    <link rel="stylesheet" href="/views/css/admin/admin-common.css">
    <link rel="stylesheet" href="/views/css/admin/product.css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/views/assets/myfonts/myfonts.css">
</head>
<body>
<section class="my-container">
    <%@include file="include/menu.jsp"%>
    <div class="dashboard">
        <%@include file="include/header.jsp"%>
        <div class="admin-header">
            <h1>Danh Mục</h1>
        </div>
        <div class="text-right p-3">
            <a href='<c:url value='${URLConstant.URL_ADMIN_CATEGORY_ADD}'></c:url>' class="btn btn-primary btn-them">
                Thêm</a>
        </div>

        <!-- CONTENT-CHANGE -->
        <div class="content">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Name</th>
                    <th scope="col">Picture</th>
                    <th scope="col">#</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="item" items="${categories}">
                <tr>
                    <th scope="row">${item.idCategory}</th>
                    <td scope="row">${item.nameCategory}</td>
                    <td scope="row">${item.linkCategory}</td>
                    <td>
                        <a href='<c:url value='${URLConstant.URL_ADMIN_CATEGORY_EDIT}?idCategory=${item.idCategory}'></c:url>' class="btn btn-success">Sửa</a>
                        <a href='<c:url value='${URLConstant.URL_ADMIN_CATEGORY_DELETE}?idCategory=${item.idCategory}'></c:url>' class="btn btn-danger">Xoá</a>
                    </td>
                </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>

        <!-- END CONTENT CHANGE -->
    </div>
</section>

<!-- SCRIPT -->
<script src="/views/js/jquery-3.2.1.slim.min.js"></script>
<script src="/views/js/popper.min.js"></script>
<script src="/views/js/bootstrap.min.js"></script>

<script>
    $(".del").click(function (event) {
        event.preventDefault();
        var r = confirm("Bạn có chắc muốn xoá sản phẩm này?");
        if (r == true) {
            window.location = $(this).attr("href");
        }
    });
</script>
</body>
</html>
