<%--
  Created by IntelliJ IDEA.
  User: Probook
  Date: 11-Dec-20
  Time: 12:15 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.bhd.util.URLConstant"%>
<html>
<head>
    <title>Đăng Nhập</title>
<%--    <link rel="stylesheet" href="../../css/user/style.css" type="text/css">--%>
    <link rel="stylesheet" href="/views/css/user/style.css" type="text/css">
<%--    <link rel="stylesheet" href="../../css/user/login.css" type="text/css">--%>
    <link rel="stylesheet" href="/views/css/user/login.css" type="text/css">
<%--    <link rel="stylesheet" href="../../assets/external/all.css" type="text/css">--%>
    <link rel="stylesheet" href="/views/assets/external/all.css" type="text/css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@600&display=swap" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/views/assets/myfonts/myfonts.css" type="text/css">
<%--    <link rel="stylesheet" href="../../assets/myfonts/myfonts.css" type="text/css">--%>
</head>
<body>
<!-- CONTENT -->
<div class="section" style="height: 100%; padding-top: 120px">
    <div class="container">
        <div id="signin-form">
            <h2>Sign in</h2>
            <form action='<c:url value='${ URLConstant.URL_ADMIN_LOGIN }'></c:url>' method="POST" onsubmit="validation()">
                <div class="form-input">
                    <input
                            type="text"
                            placeholder="username or email"
                            id="fullName"
                            name="username"
                    >
                    <span
                            id="full__name__error"
                            class="text__danger"
                            style="color: red"
                    ></span>
                </div>
                <br />
                <div class="form-input">
                    <input type="password" placeholder="password" id="password" name="password" >

                    <span
                            id="password__error"
                            class="text__danger"
                            style="color: red"
                    ></span>
                </div>
                <br />
                <div class="form-input">
                    <button type="submit">SIGN IN</button>
                </div>
            </form>
        </div>
    </div>
</div>


</body>
</html>
