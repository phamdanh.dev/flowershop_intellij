<%--
  Created by IntelliJ IDEA.
  User: Probook
  Date: 11-Dec-20
  Time: 1:38 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Chi Tiết Đơn Hàng</title>
    <link rel="stylesheet" href="../../css/admin/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/admin/admin-common.css">
    <link rel="stylesheet" href="../../css/admin/orderdetails.css">
    <link rel="stylesheet" href="../../css/admin/jquery-ui.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="../../assets/myfonts/myfonts.css">
</head>
<body>
<section class="my-container">
    <div class="menu">
        <div class="menu-name">
            <h1>John Smith</h1>
            <h6>Admin</h6>
        </div>
        <div class="menu-bar">
            <a href="">Dashboard</a>
            <a href="">Đơn hàng</a>
            <a href="">Doanh Thu</a>
            <a href="">Danh Mục</a>
            <a href="">Sản Phẩm</a>
            <a href="">Sản Phẩm Giảm Giá</a>
            <a href="">Phản Hồi</a>
            <a href="">Giao Diện</a>
            <a href="">Tài Khoản</a>
        </div>
    </div>
    <div class="dashboard">
        <div class="dashboard-header">
            <div class="account">
                <a href="">Administrator</a>
                <div class="account-option">
                    <a href="">Thông Tin Tài Khoản</a>
                    <a href="">Đăng Xuất</a>
                </div>
            </div>
        </div>
        <div class="admin-header">
            <h1>Chi Tiết Đơn Hàng</h1>
        </div>
        <!-- CONTENT-CHANGE -->
        <div class="content">
            <div class="row bg-white" style="height: 350px; width: 100%; margin: 0;">
                <div class="col-6" style="overflow-wrap: break-word;">
                    <h5>Mã Đơn Hàng: 5-23-14112020-114511</h5>
                    <h5>Username: <span><a href="" style="font-size: 1.25rem;">Nguyen Hanh</a></span></h5>
                    <h5>Tên Khách Hàng: Nguyễn Thị Hạnh</h5>
                    <h5>SDT: 0908764523</h5>
                    <h5>Email:hanhnguyen@gmail.com</h5>
                    <h5>Địa chỉ: 123 Trần Hưng Đạo, P.11, Q.5, TP.HCM</h5>
                </div>
                <div class="col-6 py-5">
                    <h5>Tổng Thanh Toán</h5>
                    <h1>1,620,000đ</h1>
                    <div class="row mt-5 d-flex">
                        <a href="" class="btn btn-success mr-3 px-5">Duyệt Đơn Hàng</a>
                        <a href="" class="btn btn-danger">Huỷ Đơn</a>
                    </div>
                </div>
            </div>
            <table class="table table-hover">
                <thead>
                <tr class="text-center">
                    <th scope="col">Mã SP</th>
                    <th scope="col">Tên SP</th>
                    <th scope="col">Số Lượng</th>
                    <th scope="col">Đơn Giá</th>
                    <th scope="col">Sale</th>
                    <th scope="col">Thành Tiền</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th scope="row">1</th>
                    <td>Hoa Yêu Thương 1</td>
                    <td>3</td>
                    <td>200.000</td>
                    <td>10%</td>
                    <td>540.000</td>
                </tr>
                <tr>
                    <th scope="row">1</th>
                    <td>Hoa Yêu Thương 1</td>
                    <td>3</td>
                    <td>200.000</td>
                    <td>10%</td>
                    <td>540.000</td>
                </tr>
                <tr>
                    <th scope="row">1</th>
                    <td>Hoa Yêu Thương 1</td>
                    <td>3</td>
                    <td>200.000</td>
                    <td>10%</td>
                    <td>540.000</td>
                </tr>

                </tbody>
            </table>
            <div class="row bg-white m-0">
                <p style="padding:30px 10px 10px;">Copyright © 2020 BHD Flower. All Rights Reserved.</p>
            </div>
        </div>

        <!-- END CONTENT CHANGE -->
    </div>

</section>


<!-- SCRIPT -->
<script src="../../js/jquery-3.2.1.slim.min.js"></script>
<script src="../../js/popper.min.js"></script>
<script src="../../js/bootstrap.min.js"></script>

</body>
</html>
