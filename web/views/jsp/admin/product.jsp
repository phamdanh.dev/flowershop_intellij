<%--
  Created by IntelliJ IDEA.
  User: Probook
  Date: 11-Dec-20
  Time: 12:57 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.bhd.util.URLConstant"%>
<html>
<head>
    <title>Sản Phẩm</title>
    <link rel="stylesheet" href="/views/css/admin/bootstrap.min.css">
    <link rel="stylesheet" href="/views/css/admin/admin-common.css">
    <link rel="stylesheet" href="/views/css/admin/product.css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/views/assets/myfonts/myfonts.css">
</head>
<body>
<section class="my-container">
    <%@include file="include/menu.jsp"%>
    <div class="dashboard">
        <%@include file="include/header.jsp"%>
        <div class="admin-header">
            <h1>Sản Phẩm</h1>
        </div>

        <!-- TOOLBAR -->
        <div class="d-flex" style="padding: 15px 20px 0px;">
            <form action="">
                <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="Search">
                    <div class="input-group-append">
                        <button class="btn btn-success" type="submit">Go</button>
                    </div>
                </div>
            </form>
            <div class="dropdown">
                <button type="button" class="btn btn-light dropdown-toggle ml-2" data-toggle="dropdown">
                    Danh Mục
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="#">Hoa Khai Trương</a>
                    <a class="dropdown-item" href="#">Hoa Tình Yêu</a>
                    <a class="dropdown-item" href="#">Hoa Cưới</a>
                    <a class="dropdown-item" href="#">Hoa Lễ</a>
                    <a class="dropdown-item" href="#">Hoa Trang Trí</a>
                    <a class="dropdown-item" href="#">Hoa Tươi</a>
                </div>
            </div>
            <div class="dropdown">
                <button type="button" class="btn btn-light dropdown-toggle ml-2" data-toggle="dropdown">
                    Sắp Xếp
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="#">Tên: A - Z</a>
                    <a class="dropdown-item" href="#">Tên: Z - A</a>
                    <a class="dropdown-item" href="#">Giá: Cao đến thấp</a>
                    <a class="dropdown-item" href="#">Giá: Thấp đến cao</a>
                </div>
            </div>
            <a href='<c:url value='${URLConstant.URL_ADMIN_PRODUCT_ADD}'></c:url>' class="btn btn-primary btn-them ml-auto" style="padding: 5px 35px;
        height: 38px">Thêm</a>
        </div>
        <!-- END TOOLBAR -->

        <!-- CONTENT-CHANGE -->
        <div class="content">
            <table class="table table-hover">
                <thead>
                <tr class="text-center">
                    <th scope="col">ID</th>
                    <th scope="col">Tên</th>
                    <th scope="col">Danh Mục</th>
                    <th scope="col">Danh Mục Con</th>
                    <th scope="col">Image</th>
                    <th scope="col">Giá</th>
                    <th scope="col">Sale</th>
                    <th scope="col">Ngày Thêm</th>
                    <th scope="col">Status</th>
                    <th scope="col">#</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="item" items="${product}">
                <tr>
                    <th scope="row">${item.id}</th>
                    <td>${item.name}</td>
                    <td>${item.nameCategory}</td>
                    <td>${item.nameSubCategory}</td>
                    <td>
                        <img src="${item.images}" alt="${item.name}" />
                    </td>
                    <td>${item.price}</td>
                    <td>${item.sale}%</td>
                    <td>${item.addDate}</td>
                    <td>${item.nameState}</td>
                    <td>
                        <a href='<c:url value='${URLConstant.URL_ADMIN_PRODUCT_EDIT}?idProduct=${item.id}'></c:url>'><button type="button"
                                                                                 class="btn btn-success">Sửa</button></a>
                        <a href='<c:url value='${URLConstant.URL_ADMIN_PRODUCT_DELETE}?idProduct=${item.id}'></c:url>' class="del"><button type="button" class="btn btn-danger">Xóa</button></a>
                        <a href='<c:url value='${URLConstant.URL_ADMIN_PRODUCT_CHANGESTATE}?idProduct=${item.id}'></c:url>'><button type="button" class="btn btn-primary" style="margin: 10px">Ẩn/Hiện</button></a>
                    </td>
                </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>

        <!-- END CONTENT CHANGE -->
    </div>
</section>

<!-- SCRIPT -->
<script src="/views/js/jquery-3.5.1.min.js"></script>
<script src="/views/js/popper.min.js"></script>
<script src="/views/js/bootstrap.min.js"></script>


<script>
    $('.del').click(function (event) {
        event.preventDefault();
        var r = confirm("Bạn có chắc muốn xoá sản phẩm này?");
        if (r == true) {
            window.location = $(this).attr('href');
        }

    });

</script>
</body>
</html>
