<%--
  Created by IntelliJ IDEA.
  User: Probook
  Date: 11-Dec-20
  Time: 12:31 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.bhd.util.URLConstant"%>
<html>
<head>
    <title>Tài Khoản</title>
    <link rel="stylesheet" href="/views/css/admin/bootstrap.min.css">
    <link rel="stylesheet" href="/views/css/admin/admin-common.css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/views/assets/myfonts/myfonts.css">
</head>
<body>
<section class="my-container">
    <%@include file="include/menu.jsp"%>
    <div class="dashboard">
        <%@include file="include/header.jsp"%>
        <div class="admin-header">
            <h1>User</h1>
        </div>
        <!-- TOOLBAR -->
        <div class="d-flex" style="padding: 15px 20px 0px;">
            <form action="">
                <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="Search">
                    <div class="input-group-append">
                        <button class="btn btn-success" type="submit">Go</button>
                    </div>
                </div>
            </form>
            <div class="dropdown">
                <button type="button" class="btn btn-light dropdown-toggle ml-2" data-toggle="dropdown">
                    Sắp Xếp
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="#">Tên: A - Z</a>
                    <a class="dropdown-item" href="#">Tên: Z - A</a>
                </div>
            </div>
            <a href='<c:url value='${URLConstant.URL_ADMIN_USER_ADD}'></c:url>' class="btn btn-primary btn-them ml-auto" style="padding: 5px 35px;
        height: 38px">Thêm</a>
        </div>
        <!-- END TOOLBAR -->
        <!-- CONTENT-CHANGE -->
        <div class="content">
            <table class="table table-hover">
                <thead>
                <tr class="text-center">
                    <th scope="col">ID</th>
                    <th scope="col">Tên</th>
                    <th scope="col">Quyền</th>
                    <th scope="col">Email</th>
                    <th scope="col">Số Điện Thoại</th>
                    <th scope="col">Địa Chỉ</th>
                    <th scope="col">#</th>
                </tr>
                </thead>
                <tbody>

                <c:forEach var="item" items="${user}">
                <tr>
                    <th scope="row">${item.id}</th>
                    <td>${item.name}</td>
                    <c:if test="${item.idRole=='0'}">
                        <td>Admin</td>
                    </c:if>
                    <c:if test="${item.idRole=='1'}">
                        <td>User</td>
                    </c:if>
                    <td>${item.email}</td>
                    <td>${item.phone}</td>
                    <td>${item.address}</td>
                    <td>
                        <a href='<c:url value='${URLConstant.URL_ADMIN_USER_EDIT}?idUser=${item.id}'></c:url>' class="btn btn-success">Sửa</a>
                        <a href='<c:url value='${URLConstant.URL_ADMIN_USER_DELETE}?idUser=${item.id}'></c:url>' class="btn btn-danger">Xóa</a>
                        <a href='<c:url value='${URLConstant.URL_ADMIN_USER_DETAILS}?idUser=${item.id}'></c:url>' class="btn btn-primary">Chi tiết</a>
                    </td>
                </tr>
                </c:forEach>

                </tbody>
            </table>

        </div>

        <!-- END CONTENT CHANGE -->
    </div>

</section>


<!-- SCRIPT -->
<script src="/views/js/jquery-3.2.1.slim.min.js"></script>
<script src="/views/js/popper.min.js"></script>
<script src="/views/js/bootstrap.min.js"></script>

</body>
</html>
