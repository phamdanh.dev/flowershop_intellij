<%--
  Created by IntelliJ IDEA.
  User: Probook
  Date: 09-Dec-20
  Time: 3:22 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.bhd.util.URLConstant"%>
<html>
<head>
    <title>Trang chủ</title>
    <link rel="stylesheet" href="/views/css/user/style.css">
    <link rel="stylesheet" href="/views/css/user/common.css">
    <link rel="stylesheet" href="/views/css/user/home.css">
    <link rel="stylesheet" href="/views/assets/external/all.css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/views/assets/myfonts/myfonts.css">
    <link rel="stylesheet" href="/views/assets/external/slick.css">
    <link rel="stylesheet" href="/views/assets/external/slick-theme.css">
</head>
<body>


<!-- HEADER -->
<%@include file="./include/header.jsp"%>

<!-- CONTENT -->

<!-- CAROUSEL -->
<section id="home-carousel">
    <div class="home-carousel">
        <div class="home-carousel-item">
            <img src="/views/images/home-carousel/home-1.jpg" alt="">
            <div class="home-carousel-text">
                <div class="home-carousel-content">
                    <h3>BHD Flower</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet, vitae?</p>
                    <a href="" class="xem-them-a">Xem thêm <i class="far fa-arrow-alt-circle-right"></i></a>
                </div>
            </div>
        </div>
        <div class="home-carousel-item">
            <img src="/views/images/home-carousel/home-2.jpg" alt="">
            <div class="home-carousel-text">
                <div class="home-carousel-content">
                    <h3>BHD Flower</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet, vitae?</p>
                    <a href="" class="xem-them-a">Xem thêm <i class="far fa-arrow-alt-circle-right"></i></a>
                </div>
            </div>
        </div>
        <div class="home-carousel-item">
            <img src="/views/images/home-carousel/home-3.jpg" alt="">
            <div class="home-carousel-text">
                <div class="home-carousel-content">
                    <h3>BHD Flower</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet, vitae?</p>
                    <a href="" class="xem-them-a">Xem thêm <i class="far fa-arrow-alt-circle-right"></i></a>
                </div>
            </div>
        </div>
        <div class="home-carousel-item">
            <img src="/views/images/home-carousel/home-4.jpg" alt="">
            <div class="home-carousel-text">
                <div class="home-carousel-content">
                    <h3>BHD Flower</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet, vitae?</p>
                    <a href="" class="xem-them-a">Xem thêm <i class="far fa-arrow-alt-circle-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- DANH MUC CAROUSEL -->
<section id="danh-muc">
    <h1>Chủ đề</h1>
    <div class="danh-muc-carousel">
        <c:forEach var="item" items="${categories}">
            <div class="danh-muc-carousel-item">

                <a href='<c:url value='${URLConstant.URL_PRODUCT_CATEGORY}?idCategory=${item.idCategory}'></c:url>'>
                    <img src="${item.linkCategory}" alt="">
                    <p>${item.nameCategory}</p>
                </a>
            </div>
        </c:forEach>

    </div>
</section>

<!-- SAN PHAM BAN CHAY -->
<section class="sp-ban-chay">
    <div class="container">
        <h1 class="product-list-h1">Sản phẩm bán chạy</h1>
        <div class="product-list">
            <div class="product-item">
                <a href=""><img src="/views/images/danh-muc/hoacuoi/CatTuong.jpg" alt=""></a>
                <br>
                <a href="">Yêu thương 1</a>
                <div class="product-item-price">
                    <span>200,000 VND</span>
                    <span><i class="fas fa-tag"></i>Sale 5%</span>
                </div>
                <div class="product-item-btn">
                    <a href="">Thêm vào giỏ</a>
                    <a href="">Mua ngay</a>
                </div>
            </div>
            <div class="product-item">
                <a href=""><img src="/views/images/danh-muc/hoakhaitruong/AnhVang.jpg" alt=""></a>
                <br>
                <a href="">Yêu thương 1</a>
                <div class="product-item-price">
                    <span>280,000 VND</span>
                    <span><i class="fas fa-tag"></i>Sale 5%</span>
                </div>
                <div class="product-item-btn">
                    <a href="">Thêm vào giỏ</a>
                    <a href="">Mua ngay</a>
                </div>
            </div>
            <div class="product-item">
                <a href=""><img src="/views/images/danh-muc/hoale/BietOn.jpg" alt=""></a>
                <br>
                <a href="">Yêu thương 1</a>
                <div class="product-item-price">
                    <span>210,000 VND</span>
                    <span><i class="fas fa-tag"></i>Sale 5%</span>
                </div>
                <div class="product-item-btn">
                    <a href="">Thêm vào giỏ</a>
                    <a href="">Mua ngay</a>
                </div>
            </div>
            <div class="product-item">
                <a href=""><img src="/views/images/danh-muc/hoatinhyeu/ChayBong.jpg" alt=""></a>
                <br>
                <a href="">Yêu thương 1</a>
                <div class="product-item-price">
                    <span>300,000 VND</span>
                    <span><i class="fas fa-tag"></i>Sale 5%</span>
                </div>
                <div class="product-item-btn">
                    <a href="">Thêm vào giỏ</a>
                    <a href="">Mua ngay</a>
                </div>
            </div>
        </div>
        <a href="" class="xem-them-a">Xem thêm <i class="far fa-arrow-alt-circle-right"></i></a>
    </div>
</section>

<!-- HOA KHAI TRUONG -->
<section class="sp-ban-chay">
    <div class="container">
        <h1 class="product-list-h1">Hoa Khai Trương</h1>
        <div class="product-list">
            <c:forEach items="${listHoaKhaiTruong}" var="item">
                <div class="product-item">
                    <a href=""><img src="${item.images}" alt="${item.name}"></a>
                    <br>
                    <a href="">${item.name}</a>
                    <div class="product-item-price">
                        <span>${item.price}</span>
                        <span><i class="fas fa-tag"></i>Sale ${item.sale}%</span>
                    </div>
                    <div class="product-item-btn">
                        <a href="">Thêm vào giỏ</a>
                        <a href="">Mua ngay</a>
                    </div>
                </div>
            </c:forEach>

<%--            <div class="product-item">--%>
<%--                <a href=""><img src="/views/images/danh-muc/hoakhaitruong/TinhKhoi.jpg" alt=""></a>--%>
<%--                <br>--%>
<%--                <a href="">Yêu thương 1</a>--%>
<%--                <div class="product-item-price">--%>
<%--                    <span>450,000 VND</span>--%>
<%--                    <span><i class="fas fa-tag"></i>Sale 5%</span>--%>
<%--                </div>--%>
<%--                <div class="product-item-btn">--%>
<%--                    <a href="">Thêm vào giỏ</a>--%>
<%--                    <a href="">Mua ngay</a>--%>
<%--                </div>--%>
<%--            </div>--%>


        </div>
        <a href="" class="xem-them-a">Xem thêm <i class="far fa-arrow-alt-circle-right"></i></a>
    </div>
</section>

<!-- HOA TINH YEU -->
<section class="sp-ban-chay">
    <div class="container">
        <h1 class="product-list-h1">Hoa Tình Yêu</h1>
        <div class="product-list">
            <c:forEach items="${listHoaTinhYeu}" var="item">
                <div class="product-item">
                    <a href=""><img src="${item.images}" alt="${item.name}"></a>
                    <br>
                    <a href="">${item.name}</a>
                    <div class="product-item-price">
                        <span>${item.price}</span>
                        <span><i class="fas fa-tag"></i>Sale ${item.sale}%</span>
                    </div>
                    <div class="product-item-btn">
                        <a href="">Thêm vào giỏ</a>
                        <a href="">Mua ngay</a>
                    </div>
                </div>
            </c:forEach>
        </div>
        <a href="" class="xem-them-a">Xem thêm <i class="far fa-arrow-alt-circle-right"></i></a>
    </div>
</section>

<!-- HOA CUOI -->
<section class="sp-ban-chay">
    <div class="container">
        <h1 class="product-list-h1">Hoa Cưới</h1>
        <div class="product-list">
            <c:forEach items="${listHoaCuoi}" var="item">
                <div class="product-item">
                    <a href=""><img src="${item.images}" alt="${item.name}"></a>
                    <br>
                    <a href="">${item.name}</a>
                    <div class="product-item-price">
                        <span>${item.price}</span>
                        <span><i class="fas fa-tag"></i>Sale ${item.sale}%</span>
                    </div>
                    <div class="product-item-btn">
                        <a href="">Thêm vào giỏ</a>
                        <a href="">Mua ngay</a>
                    </div>
                </div>
            </c:forEach>
        </div>
        <a href="" class="xem-them-a">Xem thêm <i class="far fa-arrow-alt-circle-right"></i></a>
    </div>
</section>

<!-- HOA LE -->
<section class="sp-ban-chay">
    <div class="container">
        <h1 class="product-list-h1">Hoa Lễ</h1>
        <div class="product-list">
            <c:forEach items="${listHoaLe}" var="item">
                <div class="product-item">
                    <a href=""><img src="${item.images}" alt="${item.name}"></a>
                    <br>
                    <a href="">${item.name}</a>
                    <div class="product-item-price">
                        <span>${item.price}</span>
                        <span><i class="fas fa-tag"></i>Sale ${item.sale}%</span>
                    </div>
                    <div class="product-item-btn">
                        <a href="">Thêm vào giỏ</a>
                        <a href="">Mua ngay</a>
                    </div>
                </div>
            </c:forEach>
        </div>
        <a href="" class="xem-them-a">Xem thêm <i class="far fa-arrow-alt-circle-right"></i></a>
    </div>
</section>

<!-- HOA TRANG TRI -->
<section class="sp-ban-chay">
    <div class="container">
        <h1 class="product-list-h1">Hoa Trang Trí</h1>
        <div class="product-list">
            <c:forEach items="${listHoaTrangTri}" var="item">
                <div class="product-item">
                    <a href=""><img src="${item.images}" alt="${item.name}"></a>
                    <br>
                    <a href="">${item.name}</a>
                    <div class="product-item-price">
                        <span>${item.price}</span>
                        <span><i class="fas fa-tag"></i>Sale ${item.sale}%</span>
                    </div>
                    <div class="product-item-btn">
                        <a href="">Thêm vào giỏ</a>
                        <a href="">Mua ngay</a>
                    </div>
                </div>
            </c:forEach>
        </div>
        <a href="" class="xem-them-a">Xem thêm <i class="far fa-arrow-alt-circle-right"></i></a>
    </div>
</section>

<!-- HOA TUOI -->
<section class="sp-ban-chay">
    <div class="container">
        <h1 class="product-list-h1">Hoa Tươi</h1>
        <div class="product-list">
            <c:forEach items="${listHoaTuoi}" var="item">
                <div class="product-item">
                    <a href=""><img src="${item.images}" alt="${item.name}"></a>
                    <br>
                    <a href="">${item.name}</a>
                    <div class="product-item-price">
                        <span>${item.price}</span>
                        <span><i class="fas fa-tag"></i>Sale ${item.sale}%</span>
                    </div>
                    <div class="product-item-btn">
                        <a href="">Thêm vào giỏ</a>
                        <a href="">Mua ngay</a>
                    </div>
                </div>
            </c:forEach>
        </div>
        <a href="" class="xem-them-a">Xem thêm <i class="far fa-arrow-alt-circle-right"></i></a>
    </div>
</section>

<!-- Counter -->
<section class="my-counter">
    <div class="counter-item">
        <p class="counter">32</p>
        <h4>Doanh nghiệp</h4>
    </div>
    <div class="counter-item">
        <p class="counter">1</p>
        <h4>Năm hoạt động</h4>
    </div>
    <div class="counter-item">
        <p class="counter">25</p>
        <h4>Thủ công</h4>
    </div>
    <div class="counter-item">
        <p class="counter">64</p>
        <h4>Tỉnh thành</h4>
    </div>
</section>


<!-- CONTACT -->
<div id="footer-contact">
    <div class="container">
        <h1>Liên Hệ</h1>
        <div id="f-contact-list">
            <div class="f-contact-item">
                <h3>Địa Chỉ</h3>
                <p>231 Đường số 32, P.Bình Trị Đông B, Q.Bình Tân, TP.HCM</p>
            </div>
            <div class="f-contact-item">
                <h3>Số Điện Thoại</h3>
                <p>08 9808 2836 - Mr.Danh</p>
            </div>
            <div class="f-contact-item">
                <h3>Email</h3>
                <p>contact.bhdflower@gmail.com</p>
            </div>
            <div class="f-contact-item">
                <h3>Giờ Làm Việc</h3>
                <p>5h30 - 22h30</p>
                <p>Tất cả các ngày trong tuần</p>
            </div>
        </div>
    </div>
</div>

<!-- MAP -->
<iframe
        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.7843494480253!2d106.6064945142869!3d10.751096762601838!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752c34d6561c0f%3A0xc0abbaf11df72ff6!2zMjMxIMSQxrDhu51uZyBz4buRIDMyIFRp4buDdSBLaHUgMSwgQsOsbmggVHLhu4sgxJDDtG5nIEIsIELDrG5oIFTDom4sIFRow6BuaCBwaOG7kSBI4buTIENow60gTWluaCwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1603097034851!5m2!1svi!2s"
        width="100%" height="320" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false"
        tabindex="0"></iframe>


<!-- FOOTER -->
<%@include file="./include/footer.jsp"%>

<!-- SCRIPT -->
<script src="/views/js/jquery-3.5.1.min.js"></script>
<script src="/views/js/jquery.waypoints.min.js"></script>
<script src="/views/js/slick.min.js"></script>
<script src="/views/js/jquery.countup.min.js"></script>
<!-- SCRIPT CODE  -->
<script>
    $('.counter').countUp({
        'time': 2000,
        'delay': 10
    });
</script>
<script>
    $('.danh-muc-carousel').slick({
        dots: false,
        arrows: false,
        infinite: true,
        speed: 700,
        slidesToShow: 1,
        centerMode: true,
        variableWidth: true,
        autoplay: true,
        autoplaySpeed: 2000
    });
</script>
<script>
    $('.home-carousel').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });
</script>
</body>
</html>
