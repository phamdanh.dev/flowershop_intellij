<%--
  Created by IntelliJ IDEA.
  User: Probook
  Date: 05-Jan-21
  Time: 4:09 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="/views/css/user/style.css">
    <link rel="stylesheet" href="/views/css/user/common.css">
    <link rel="stylesheet" href="/views/css/user/home.css">
    <link rel="stylesheet" href="/views/assets/external/all.css">
</head>
<body>
<footer>
    <div id="footer">
        <div class="container">
            <div id="footer-1">
                <div class="f-1-item">
                    <img src="/views/images/logo.PNG" alt="">
                </div>
                <div class="f-1-item">
                    <h3>Hỗ Trợ</h3>
                    <ul>
                        <li><a href="">Ship Hàng Miễn Phí</a></li>
                        <li><a href="">Hướng Dẫn Mua Hàng</a></li>
                        <li><a href="">Phương Thức Thanh Toán</a></li>
                        <li><a href="">Phương Thức Giao Hàng</a></li>
                    </ul>
                </div>
                <div class="f-1-item">
                    <h3>Chăm Sóc Khách Hàng</h3>
                    <img src="/views/images/logo.PNG" alt="">
                </div>
                <div class="f-1-item">
                    <h3>Liên Kết Nhanh</h3>
                    <ul>
                        <li><a href="">Trang Chủ</a></li>
                        <li><a href="">Sản Phẩm</a></li>
                        <li><a href="">Giới Thiệu</a></li>
                        <li><a href="">Liên Hệ</a></li>
                    </ul>
                </div>
            </div>
            <div id="footer-2">
                <div id="f-2-left">
                    <h3>CỬA HÀNG HOA BHD FLOWER</h3>
                    <p>Địa chỉ: 231 Đường số 32, P.Bình Trị Đông B, Q.Bình Tân, TP.HCM</p>
                    <p>Hotline: 08 9808 2836 Mr Danh</p>
                    <p>Email: bhdflower@gmail.com</p>
                </div>
                <div id="f-2-right">
                    <a href=""><i class="fab fa-instagram-square"></i></a>
                    <a href=""><i class="fab fa-facebook"></i></a>
                    <a href=""><i class="fab fa-twitter-square"></i></a>
                    <a href=""><i class="fab fa-youtube-square"></i></a>
                </div>

            </div>
            <div id="footer-3">
                <div>
                    <p>Copyright &copy 2020 BHD Flower. All Rights Reserved.</p>
                </div>
                <div>
                    <p>GitLab <i class="fab fa-git-square"></i> <i class="fab fa-gitlab"></i> : <span><a
                            href="https://tinyurl.com/y2pkz68p">https://gitlab.com/bhdflower</a></span></p>
                </div>
            </div>
        </div>
    </div>
</footer>
</body>
</html>
