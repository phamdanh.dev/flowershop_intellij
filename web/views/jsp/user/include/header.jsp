<%--
  Created by IntelliJ IDEA.
  User: Probook
  Date: 05-Jan-21
  Time: 4:04 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.bhd.util.URLConstant"%>
<html>
<head>
    <link rel="stylesheet" href="/views/css/user/style.css">
    <link rel="stylesheet" href="/views/css/user/common.css">
    <link rel="stylesheet" href="/views/css/user/home.css">
    <link rel="stylesheet" href="/views/assets/external/all.css">
</head>
<body>
<!-- HEADER -->
<header id="header">
    <div class="container">
        <div id="header-img">
            <a href=""><img src="/views/images/logo.PNG" alt="bhd-flower-logo"></a>
        </div>
        <nav>
            <ul>
                <li><a href="./home.html">Trang Chủ</a></li>
                <li id="sp-hover"><a href="./product.html">Sản Phẩm</a>

                    <div id="sp-hover-list">
<%--                        <ul>--%>
<%--                            <li><a href="">HOA KHAI TRƯƠNG</a></li>--%>
<%--                            <li><a href="">HOA TÌNH YÊU</a></li>--%>
<%--                            <li><a href="">HOA CƯỚI</a></li>--%>
<%--                            <li><a href="">HOA LỄ</a></li>--%>
<%--                            <li><a href="">HOA TRANG TRÍ</a></li>--%>
<%--                            <li id="sp-hover-2"><a href="">HOA TƯƠI</a>--%>
<%--                                <div id="sp-hover-list-2">--%>
<%--                                    <ul>--%>
<%--                                        <li><a href="">HOA HỒNG</a></li>--%>
<%--                                        <li><a href="">HOA LY</a></li>--%>
<%--                                        <li><a href="">HOA SEN</a></li>--%>
<%--                                        <li><a href="">HOA HƯỚNG DƯƠNG</a></li>--%>
<%--                                        <li><a href="">HOA LAN</a></li>--%>
<%--                                    </ul>--%>
<%--                                </div>--%>
<%--                            </li>--%>
<%--                        </ul>--%>
                        <ul>
                            <c:forEach items="${categories}" var="item">
                                    <li><a href='<c:url value='${URLConstant.URL_PRODUCT_CATEGORY}?idCategory=${id.nameCategory}'></c:url>'>${item.nameCategory}</a></li>
                            </c:forEach>
                        </ul>
                    </div>
                </li>
                <li><a href="./introduction.html">Giới Thiệu</a></li>
                <li><a href="./contact.html">Liên Hệ</a></li>
            </ul>
        </nav>
        <div id="header-search">
            <form action="">
                <input type="text" name="" id="" placeholder="Bạn tìm hoa gì?">
                <button><i class="fas fa-search"></i></button>
            </form>
        </div>
        <div id="header-cart">
            <a href=""><i class="fas fa-shopping-cart"></i></a>
        </div>
        <div id="header-login">

            <%
                String username = (String)session.getAttribute("USER_NAME");
                if (username != null) {
            %>
            <p>${sessionScope.USER_NAME}</p>
            <ul>
                <li><a href='<c:url value='${URLConstant.URL_USER_ACCOUNT}'></c:url>'>Tài khoản</a></li>
                <li><a href='<c:url value='${URLConstant.URL_LOGOUT}'></c:url>'>Đăng xuất</a></li>
            </ul>
            <%
            } else {
            %>
            <i class="fas fa-lock"></i>
            <p>LOGIN</p>
            <ul>
                <li><a href='<c:url value='${URLConstant.URL_LOGIN}'></c:url>'>Đăng nhập</a></li>
                <li><a href='<c:url value='${URLConstant.URL_REGISTER}'></c:url>'>Đăng kí mới</a></li>
            </ul>
            <%
                }
            %>
        </div>
    </div>
</header>
<div id="header-clear"></div>
<!-- END HEADER -->
</body>
</html>
