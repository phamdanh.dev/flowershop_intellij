<%--
  Created by IntelliJ IDEA.
  User: Probook
  Date: 11-Dec-20
  Time: 12:15 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.bhd.util.URLConstant"%>
<html>
<head>
    <title>Đăng Nhập</title>
<%--    <link rel="stylesheet" href="../../css/user/style.css" type="text/css">--%>
    <link rel="stylesheet" href="/views/css/user/style.css" type="text/css">
<%--    <link rel="stylesheet" href="../../css/user/login.css" type="text/css">--%>
    <link rel="stylesheet" href="/views/css/user/login.css" type="text/css">
<%--    <link rel="stylesheet" href="../../assets/external/all.css" type="text/css">--%>
    <link rel="stylesheet" href="/views/assets/external/all.css" type="text/css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300&display=swap" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@600&display=swap" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/views/assets/myfonts/myfonts.css" type="text/css">
<%--    <link rel="stylesheet" href="../../assets/myfonts/myfonts.css" type="text/css">--%>
</head>
<body>
<!-- CONTENT -->
<div class="section">
    <div class="container">
        <div id="signin-form">
            <h2>Sign in</h2>
            <form action='<c:url value='${ URLConstant.URL_LOGIN }'></c:url>' method="POST" onsubmit="validation()">
                <div class="form-input">
                    <input
                            type="text"
                            placeholder="Phone"
                            id="fullName"
                            name="username"
                    >
                </div>
                <br />
                <div class="form-input">
                    <input type="password" placeholder="password" id="password" name="password" >
                </div>
                <br />
                <div class="form-input">
                    <button type="submit">SIGN IN</button>
                </div>
            </form>
            <p>Or login with</p>
            <div id="icon">
                <a href=""><i class="fab fa-facebook-f"></i></a>
                <a href=""> <i class="fab fa-google-plus-g"></i></a>
            </div>
            <a href='<c:url value='${URLConstant.URL_REGISTER}'></c:url>'>Sign Up</a>
        </div>
    </div>
</div>

<!-- CONTACT -->
<div id="footer-contact">
    <div class="container">
        <h1>Liên Hệ</h1>
        <div id="f-contact-list">
            <div class="f-contact-item">
                <h3>Địa Chỉ</h3>
                <p>231 Đường số 32, P.Bình Trị Đông B, Q.Bình Tân, TP.HCM</p>
            </div>
            <div class="f-contact-item">
                <h3>Số Điện Thoại</h3>
                <p>08 9808 2836 - Mr.Danh</p>
            </div>
            <div class="f-contact-item">
                <h3>Email</h3>
                <p>contact.bhdflower@gmail.com</p>
            </div>
            <div class="f-contact-item">
                <h3>Giờ Làm Việc</h3>
                <p>5h30 - 22h30</p>
                <p>Tất cả các ngày trong tuần</p>
            </div>
        </div>
    </div>
</div>

<!-- MAP -->
<iframe
        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.7843494480253!2d106.6064945142869!3d10.751096762601838!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752c34d6561c0f%3A0xc0abbaf11df72ff6!2zMjMxIMSQxrDhu51uZyBz4buRIDMyIFRp4buDdSBLaHUgMSwgQsOsbmggVHLhu4sgxJDDtG5nIEIsIELDrG5oIFTDom4sIFRow6BuaCBwaOG7kSBI4buTIENow60gTWluaCwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1603097034851!5m2!1svi!2s"
        width="100%"
        height="320"
        frameborder="0"
        style="border: 0"
        allowfullscreen=""
        aria-hidden="false"
        tabindex="0"
></iframe>

<!-- FOOTER -->
<%@include file="./include/footer.jsp"%>

</body>
</html>
